package com.pde.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import sun.org.mozilla.javascript.internal.Delegator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.SplitPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.pde.game.Editor.ActionType;
import com.pde.game.Item.ItemType;
import com.pde.game.Level.Layer;
import com.pde.game.Utils.Direction;

public class EditorGUI implements InputProcessor
{
	private Stage stage;
	private Skin skin;
	private World world;
	private Editor editor;
	
	private MapLoader mapLoader;
	
	public EditorGUI(World world)
	{
		this.world = world;
		this.editor = world.getEditor();
		
		this.mapLoader = new MapLoader(world);
		
		skin = new Skin(Gdx.files.internal("gui/uiskin.json"));	
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		
		ArrayList<String> buildObjectList = new ArrayList<String>();
		final Map<String, Sprite> buildObjectSprites = new HashMap<String, Sprite>(); 
		
		for(Entry<String, GameObject> entry : editor.getBuildObjects().entrySet())
		{
			buildObjectList.add(entry.getKey());
			buildObjectSprites.put(entry.getKey(), SpriteManager.getSprite(entry.getKey()));
		}
		
		Label lblActionType = new Label("Action Type:", skin);
		Label lblBuildObjectType = new Label("Build Object Type:", skin);
		Label lblNavigation = new Label("Navigation:", skin);
		
		Label lblSubtype = new Label("Subtype:", skin);
		Label lblLayer = new Label("Layer:", skin);
		Label lblDirection = new Label("Direction:", skin);
		Label lblSpecific = new Label("Specific:", skin);
		
		Label lblCursorX = new Label("Cursor position X:", skin);
		Label lblCursorY = new Label("Cursor position Y:", skin);
		Label lblCameraX = new Label("Camera position X:", skin);
		Label lblCameraY = new Label("Camera position Y:", skin);
		Label lblMapSizeX = new Label("Map size X:", skin);
		Label lblMapSizeY = new Label("Map size Y:", skin);
		
		Label lblCursorXRes = new Label("0", skin);
		Label lblCursorYRes = new Label("0", skin);
		Label lblCameraXRes = new Label("0", skin);
		Label lblCameraYRes = new Label("0", skin);
		Label lblMapSizeXRes = new Label("0", skin);
		Label lblMapSizeYRes = new Label("0", skin);
		
		final Button btnBuildAction = new TextButton("Build", skin, "toggle");
		btnBuildAction.setChecked(true);
		final Button btnClearAction = new TextButton("Clear", skin, "toggle");
		final Button btnDebugAction = new TextButton("Debug", skin, "toggle");
		Button btnLoadMapAction = new TextButton("Load\n Map", skin);
		Button btnSaveMapAction = new TextButton("Save\n Map", skin);
		final Button btnGameAction = new TextButton("Game", skin);
		
		Texture texNavUp = new Texture(Gdx.files.internal("gui/nav_up.png"));
		TextureRegion imgNavUp = new TextureRegion(texNavUp);
		Texture texNavDown = new Texture(Gdx.files.internal("gui/nav_down.png"));
		TextureRegion imgNavDown = new TextureRegion(texNavDown);
		Texture texNavLeft = new Texture(Gdx.files.internal("gui/nav_left.png"));
		TextureRegion imgNavLeft = new TextureRegion(texNavLeft);
		Texture texNavRight = new Texture(Gdx.files.internal("gui/nav_right.png"));
		TextureRegion imgNavRight = new TextureRegion(texNavRight);
		Texture texNavRes = new Texture(Gdx.files.internal("gui/nav_res.png"));
		TextureRegion imgNavRes = new TextureRegion(texNavRes);
		
		Button btnUp = new Button(new Image(imgNavUp), skin);
		Button btnDown = new Button(new Image(imgNavDown), skin);
		Button btnLeft = new Button(new Image(imgNavLeft), skin);
		Button btnRight = new Button(new Image(imgNavRight), skin);
		Button btnRes = new Button(new Image(imgNavRes), skin);
		
		final SelectBox sbBuildObjectType = new SelectBox(buildObjectList.toArray(), skin);
		final SelectBox sbSubtype = new SelectBox(new String[] {""}, skin);
		sbSubtype.setTouchable(Touchable.disabled);
		
		final SelectBox sbLayer = new SelectBox(new String[] {""}, skin);
		sbLayer.setTouchable(Touchable.disabled);
		
		final SelectBox sbDirection = new SelectBox(new String[] {""}, skin);
		sbDirection.setTouchable(Touchable.disabled);
		
		final SelectBox sbSpecific = new SelectBox(new String[] {""}, skin);
		sbSpecific.setTouchable(Touchable.disabled);
		
		final TextField tfSpecific = new TextField("", skin);
		
		Image imgObjectType = new Image();
		final ScrollPane spObjectImage = new ScrollPane(imgObjectType);
		spObjectImage.setScrollingDisabled(true, true);
		
		Table tabActionType = new Table();
		tabActionType.add(btnBuildAction).width(70).height(55);
		tabActionType.add(btnClearAction).width(70).height(55);
		tabActionType.add(btnDebugAction).width(70).height(55);
		tabActionType.add(btnLoadMapAction).width(70).height(55);
		tabActionType.add(btnSaveMapAction).width(70).height(55);
		tabActionType.add(btnGameAction).width(70).height(55);
		
		Table tabBuildObjectTypeLeft = new Table();
		tabBuildObjectTypeLeft.add(sbBuildObjectType).width(150);
		tabBuildObjectTypeLeft.row();
		tabBuildObjectTypeLeft.add().height(10);
		tabBuildObjectTypeLeft.row();
		tabBuildObjectTypeLeft.add(spObjectImage).width(140).height(140);
		tabBuildObjectTypeLeft.row();
		
		Table tabBuildObjectTypeRight = new Table();
		tabBuildObjectTypeRight.add(lblSubtype).left();
		tabBuildObjectTypeRight.add(sbSubtype).width(150);
		tabBuildObjectTypeRight.row();
		tabBuildObjectTypeRight.add(lblLayer).left();
		tabBuildObjectTypeRight.add(sbLayer).width(150);
		tabBuildObjectTypeRight.row();
		tabBuildObjectTypeRight.add(lblDirection).left();
		tabBuildObjectTypeRight.add(sbDirection).width(150);
		tabBuildObjectTypeRight.row();
		tabBuildObjectTypeRight.add(lblSpecific).left();
		tabBuildObjectTypeRight.add(sbSpecific).width(150);
		tabBuildObjectTypeRight.row();
		tabBuildObjectTypeRight.add().left();
		tabBuildObjectTypeRight.add(tfSpecific).width(150);
		
		Table tabBuildObjectType = new Table();
		tabBuildObjectType.add(tabBuildObjectTypeLeft);
		tabBuildObjectType.add().width(10);
		tabBuildObjectType.add(tabBuildObjectTypeRight).left().top();
		
		Table tabNavigationLeft = new Table();
		tabNavigationLeft.add(lblCursorX).left();
		tabNavigationLeft.add(lblCursorXRes).left().padLeft(5).maxWidth(50);
		tabNavigationLeft.row();
		tabNavigationLeft.add(lblCursorY).left();
		tabNavigationLeft.add(lblCursorYRes).left().padLeft(5).maxWidth(50);
		tabNavigationLeft.row();
		tabNavigationLeft.add(lblCameraX).left();
		tabNavigationLeft.add(lblCameraXRes).left().padLeft(5).maxWidth(50);	
		tabNavigationLeft.row();
		tabNavigationLeft.add(lblCameraY).left();
		tabNavigationLeft.add(lblCameraYRes).left().padLeft(5).maxWidth(50);
		tabNavigationLeft.row();
		tabNavigationLeft.add(lblMapSizeX).left();
		tabNavigationLeft.add(lblMapSizeXRes).left().padLeft(5).maxWidth(50);
		tabNavigationLeft.row();
		tabNavigationLeft.add(lblMapSizeY).left();
		tabNavigationLeft.add(lblMapSizeYRes).left().padLeft(5).maxWidth(50);
		
		Table tabNavigationRight = new Table();
		tabNavigationRight.add(btnUp).padLeft(100).colspan(3);
		tabNavigationRight.row();
		tabNavigationRight.add(btnLeft).padLeft(100);
		tabNavigationRight.add(btnRes).left();
		tabNavigationRight.add(btnRight).left();
		tabNavigationRight.row();
		tabNavigationRight.add(btnDown).padLeft(100).colspan(3);		
		
		Table tabNavigation = new Table();
		tabNavigation.add(tabNavigationLeft);
		tabNavigation.add(tabNavigationRight);
		
	    Window window = new Window("Editor", skin);
	    window.defaults().left();
		window.add(lblActionType);
		window.row();
		window.add().height(10);
		window.row();
		window.add(tabActionType);
		window.row();
		window.add().height(10);
		window.row();
		window.add(lblBuildObjectType);
		window.row();
		window.add().height(10);
		window.row();
		window.add(tabBuildObjectType);
		window.row();
		window.add().height(10);
		window.row();
		window.add(lblNavigation);
		window.row();
		window.add().height(10);
		window.row();
		window.add(tabNavigation);
		
		window.pack();
				
		btnBuildAction.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				btnClearAction.setChecked(false);
				editor.setActionType(ActionType.BUILD);
			}
		});
		
		btnClearAction.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				btnBuildAction.setChecked(false);
				editor.setActionType(ActionType.CLEAR);
			}
		});
		
		btnDebugAction.addListener(new ChangeListener() 
		{
			@Override
			public void changed (ChangeEvent event, Actor actor) 
			{
				Settings.DEBUG_MODE = btnDebugAction.isChecked();
			}
		});
		
		btnGameAction.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Settings.EDITOR_MODE = !btnGameAction.isChecked();				
			}
		});
		
		btnSaveMapAction.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				mapLoader.saveMap(Gdx.files.external("Kindrem/maps/dev_map01.xml"));
			}
		});
		
		btnLoadMapAction.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				mapLoader.loadMap(Gdx.files.external("Kindrem/maps/dev_map01.xml"));
			}
		});
		
		btnLeft.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				editor.setPosition(editor.getPosition().x - 1, editor.getPosition().y);
			}
		});
		
		btnRight.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				editor.setPosition(editor.getPosition().x + 1, editor.getPosition().y);
			}
		});
		
		btnUp.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				editor.setPosition(editor.getPosition().x, editor.getPosition().y + 1);
			}
		});
		
		btnDown.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				editor.setPosition(editor.getPosition().x, editor.getPosition().y - 1);
			}
		});
		
		btnRes.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				editor.setPosition(0, 0);
			}
		});
		
		// TODO wywalic hardcode
		sbBuildObjectType.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				String selectedObject = sbBuildObjectType.getSelection();
				GameObject objectToBuild = editor.getBuildObjects().get(selectedObject);
				
				editor.setBuildObjectType(objectToBuild.getGameObjectType());
						
				switch(objectToBuild.getGameObjectType())
				{
					case BLOCK:
					{
						Block block = (Block)objectToBuild;
						
						Sprite spr = buildObjectSprites.get("block");
						
						spr.setAnimation("" + block.getType());
						spObjectImage.setWidget(new Image(spr.getCurrentFrame()));
						
						ArrayList<String> subTypeList = new ArrayList<String>();
						
						for(int i = 0; i < 14; i++)
						{
							if(i < 10)
							{
								subTypeList.add("block_0" + i);
							}
							else
							{
								subTypeList.add("block_" + i);
							}
						}
						
						sbSubtype.setItems(subTypeList.toArray());
						sbSubtype.setSelection(0);
						sbSubtype.setTouchable(Touchable.enabled);
						
						ArrayList<String> layersList = new ArrayList<String>();
						layersList.add("Level");
						layersList.add("Background");
						layersList.add("Foreground");
						
						sbLayer.setItems(layersList.toArray());
						sbLayer.setSelection(0);
						sbLayer.setTouchable(Touchable.enabled);
						
						ArrayList emptyList = new ArrayList();
						sbDirection.setItems(emptyList.toArray());
						sbDirection.setSelection(0);
						sbDirection.setTouchable(Touchable.disabled);
						
						ArrayList<String> specificList = new ArrayList<String>();
						specificList.add("Collidable");
						specificList.add("Trap");
						specificList.add("Ladder");
						sbSpecific.setItems(specificList.toArray());
						sbSpecific.setSelection(0);
						sbSpecific.setTouchable(Touchable.enabled);
						
						sbDirection.setTouchable(Touchable.disabled);
						
						break;
					}
					case ENEMY:
					{
						Enemy enemy = (Enemy)objectToBuild;
						
						Sprite spr = buildObjectSprites.get("enemy");
						
						if(enemy.isFacingLeft())
						{
							spr.setAnimation("idleLeft");
						}
						else
						{
							spr.setAnimation("idleRight");
						}
						
						spObjectImage.setWidget(new Image(spr.getCurrentFrame()));
						
						ArrayList emptyList = new ArrayList();
						
						sbSubtype.setItems(emptyList.toArray());
						sbSubtype.setSelection(0);
						sbSubtype.setTouchable(Touchable.disabled);
						
						sbLayer.setItems(emptyList.toArray());
						sbLayer.setSelection(0);
						sbLayer.setTouchable(Touchable.disabled);
						
						sbDirection.setItems(emptyList.toArray());
						sbDirection.setSelection(0);
						sbDirection.setTouchable(Touchable.disabled);
						
						ArrayList<String> specificList = new ArrayList<String>();
						specificList.add("Facing left");
						sbSpecific.setItems(specificList.toArray());
						sbSpecific.setSelection(0);
						sbSpecific.setTouchable(Touchable.enabled);
						
						break;
					}
					case CANNON:
					{
						Cannon cannon = (Cannon)objectToBuild;
						
						Sprite spr = buildObjectSprites.get("cannon");
						
						if(cannon.getDirection().equals(Direction.LEFT))
						{
							spr.setAnimation("idleLeft");
						}
						else if(cannon.getDirection().equals(Direction.RIGHT))
						{
							spr.setAnimation("idleRight");
						}
						else if(cannon.getDirection().equals(Direction.UP))
						{
							spr.setAnimation("idleUp");
						}
						else if(cannon.getDirection().equals(Direction.DOWN))
						{
							spr.setAnimation("idleDown");
						}
						
						spObjectImage.setWidget(new Image(spr.getCurrentFrame()));
						
						ArrayList emptyList = new ArrayList();
						
						sbSubtype.setItems(emptyList.toArray());
						sbSubtype.setSelection(0);
						sbSubtype.setTouchable(Touchable.disabled);
						
						sbLayer.setItems(emptyList.toArray());
						sbLayer.setSelection(0);
						sbLayer.setTouchable(Touchable.disabled);
						
						ArrayList<String> directionList = new ArrayList<String>();
						directionList.add("Left");
						directionList.add("Right");
						directionList.add("Up");
						directionList.add("Down");
						sbDirection.setItems(directionList.toArray());
						sbDirection.setSelection(0);
						sbDirection.setTouchable(Touchable.enabled);
						
						ArrayList<String> specificList = new ArrayList<String>();
						specificList.add("Shot delay");
						sbSpecific.setItems(specificList.toArray());
						sbSpecific.setSelection(0);
						sbSpecific.setTouchable(Touchable.enabled);
						
						break;
					}
					case ITEM:
					{
						Item item = (Item)objectToBuild;
						Sprite spr = buildObjectSprites.get("item");
						
						switch(item.getItemType())
						{
							case ITEM_COIN:
							{
								spr.setAnimation("item_coin");
								break;
							}
							case ITEM_HEART:
							{
								spr.setAnimation("item_heart_red");
								break;
							}
							case ITEM_DOOR_GREEN:
							{
								spr.setAnimation("item_door_green_closed");
								break;
							}
							case ITEM_DOOR_BROWN:
							{
								spr.setAnimation("item_door_brown_closed");
								break;
							}
							case ITEM_DOOR_SILVER:
							{
								spr.setAnimation("item_door_silver_closed");
								break;
							}
							case ITEM_DOOR_GOLD:
							{
								spr.setAnimation("item_door_gold_closed");
								break;
							}
							case ITEM_KEY_GREEN:
							{
								spr.setAnimation("item_key_green");
								break;
							}
							case ITEM_KEY_BROWN:
							{
								spr.setAnimation("item_key_brown");
								break;
							}
							case ITEM_KEY_SILVER:
							{
								spr.setAnimation("item_key_silver");
								break;	
							}
							case ITEM_KEY_GOLD:
							{
								spr.setAnimation("item_key_gold");
								break;	
							}
						}
						
						spObjectImage.setWidget(new Image(spr.getCurrentFrame()));
						
						ArrayList subtypeList = new ArrayList();
						subtypeList.add("coin");
						subtypeList.add("heart");
						subtypeList.add("door_green");
						subtypeList.add("door_brown");
						subtypeList.add("door_silver");
						subtypeList.add("door_gold");
						subtypeList.add("key_green");
						subtypeList.add("key_brown");
						subtypeList.add("key_silver");
						subtypeList.add("key_gold");
						
						sbSubtype.setItems(subtypeList.toArray());
						sbSubtype.setSelection(0);
						sbSubtype.setTouchable(Touchable.enabled);
						
						ArrayList emptyList = new ArrayList();
						
						sbDirection.setItems(emptyList.toArray());
						sbDirection.setTouchable(Touchable.disabled);
						
						sbLayer.setItems(emptyList.toArray());
						sbLayer.setTouchable(Touchable.disabled);
						
						ArrayList<String> specificList = new ArrayList<String>();
						specificList.add("ID");
						sbSpecific.setItems(specificList.toArray());
						sbSpecific.setSelection(0);
						sbSpecific.setTouchable(Touchable.enabled);
					}
				}
			}
		});
		
		sbSubtype.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				String buildObjectTypeSelectionStr = sbBuildObjectType.getSelection();
				String subtypeSelectionStr = sbSubtype.getSelection();
				
				if(buildObjectTypeSelectionStr.contains("block"))
				{
					Sprite spr = buildObjectSprites.get("block");					

					Block block = (Block)editor.getBuildObjects().get(buildObjectTypeSelectionStr);
					int blockSubtype = Integer.parseInt(subtypeSelectionStr.split("_")[1]);
					
					block.setType(blockSubtype);
					spr.setAnimation("" + blockSubtype);
					
					spObjectImage.setWidget(new Image(spr.getCurrentFrame()));
				}
				else if(buildObjectTypeSelectionStr.contains("item"))
				{
					Item item = (Item)editor.getBuildObjects().get(buildObjectTypeSelectionStr);
					Sprite spr = buildObjectSprites.get("item");
					
					if(subtypeSelectionStr.equals("coin"))
					{
						spr.setAnimation("item_coin");
						item.setItemType(ItemType.ITEM_COIN);
					}
					else if(subtypeSelectionStr.equals("heart"))
					{
						spr.setAnimation("item_heart_red");
						item.setItemType(ItemType.ITEM_HEART);
					}
					else if(subtypeSelectionStr.equals("door_green"))
					{
						spr.setAnimation("item_door_green_closed");
						item.setItemType(ItemType.ITEM_DOOR_GREEN);
					}
					else if(subtypeSelectionStr.equals("door_brown"))
					{
						spr.setAnimation("item_door_brown_closed");
						item.setItemType(ItemType.ITEM_DOOR_BROWN);
					}
					else if(subtypeSelectionStr.equals("door_silver"))
					{
						spr.setAnimation("item_door_silver_closed");
						item.setItemType(ItemType.ITEM_DOOR_SILVER);
					}
					else if(subtypeSelectionStr.equals("door_gold"))
					{
						spr.setAnimation("item_door_gold_closed");
						item.setItemType(ItemType.ITEM_DOOR_GOLD);
					}
					else if(subtypeSelectionStr.equals("key_green"))
					{
						spr.setAnimation("item_key_green");
						item.setItemType(ItemType.ITEM_KEY_GREEN);
					}
					else if(subtypeSelectionStr.equals("key_brown"))
					{
						spr.setAnimation("item_key_brown");
						item.setItemType(ItemType.ITEM_KEY_BROWN);
					}
					else if(subtypeSelectionStr.equals("key_silver"))
					{
						spr.setAnimation("item_key_silver");
						item.setItemType(ItemType.ITEM_KEY_SILVER);
					}
					else if(subtypeSelectionStr.equals("key_gold"))
					{
						spr.setAnimation("item_key_gold");
						item.setItemType(ItemType.ITEM_KEY_GOLD);
					}
					
					spObjectImage.setWidget(new Image(spr.getCurrentFrame()));
				}
			}
		});
		
		sbLayer.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				String buildObjectTypeSelectionStr = sbBuildObjectType.getSelection();
				String layerSelectionStr = sbLayer.getSelection();
				
				System.out.println("layer change");
				
				if(buildObjectTypeSelectionStr.equals("block"))
				{					
					if(layerSelectionStr.equals("Background"))
					{
						editor.setCurrentLayer(Layer.BACKGROUND);
					}
					else if(layerSelectionStr.equals("Level"))
					{
						editor.setCurrentLayer(Layer.LEVEL);
					}
					else if(layerSelectionStr.equals("Foreground"))
					{
						editor.setCurrentLayer(Layer.FOREGROUND);
					}
				}
			}
		});
		
		sbDirection.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				String buildObjectTypeSelectionStr = sbBuildObjectType.getSelection();
				String directionSelectionStr = sbDirection.getSelection();
				
				if(buildObjectTypeSelectionStr.equals("cannon"))
				{
					Cannon cannon = (Cannon)editor.getBuildObjects().get("cannon");
					Sprite spr = buildObjectSprites.get("cannon");
					
					if(directionSelectionStr.equals("Left"))
					{
						cannon.setDirection(Direction.LEFT);
						spr.setAnimation("idleLeft");
					}
					else if(directionSelectionStr.equals("Right"))
					{
						cannon.setDirection(Direction.RIGHT);
						spr.setAnimation("idleRight");
					}
					else if(directionSelectionStr.equals("Up"))
					{
						cannon.setDirection(Direction.UP);
						spr.setAnimation("idleUp");
					}
					else if(directionSelectionStr.equals("Down"))
					{
						cannon.setDirection(Direction.DOWN);
						spr.setAnimation("idleDown");
					}
					
					spObjectImage.setWidget(new Image(spr.getCurrentFrame()));
				}
			}
		});
		
		sbSpecific.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				String buildObjectTypeSelectionStr = sbBuildObjectType.getSelection();
				String specificSelectionStr = sbSpecific.getSelection();
							
				if(buildObjectTypeSelectionStr.equals("block"))
				{
					Block block = (Block)editor.getBuildObjects().get("block");
					
					if(specificSelectionStr.equals("Collidable"))
					{
						String specificValue = String.valueOf(block.isCollidable());
						tfSpecific.setText(specificValue);
						
						return;
					}
					else if(specificSelectionStr.equals("Trap"))
					{
						String specificValue = String.valueOf(block.isTrap());
						tfSpecific.setText(specificValue);
						
						return;
					}
					else if(specificSelectionStr.equals("Ladder"))
					{
						String specificValue = String.valueOf(block.isLadder());
						tfSpecific.setText(specificValue);
						
						return;
					}
				}
				else if(buildObjectTypeSelectionStr.equals("enemy"))
				{
					Enemy enemy = (Enemy)editor.getBuildObjects().get("enemy");
					
					if(specificSelectionStr.equals("Facing left"))
					{
						String specificValue = String.valueOf(enemy.isFacingLeft());
						tfSpecific.setText(specificValue);
						
						return;
					}
				}
				else if(buildObjectTypeSelectionStr.equals("cannon"))
				{
					Cannon cannon = (Cannon)editor.getBuildObjects().get("cannon");
					
					if(specificSelectionStr.equals("Shot delay"))
					{
						String specificValue = String.valueOf(cannon.getShotDelay());
						tfSpecific.setText(specificValue);
						
						return;
					}
				}
				else if(buildObjectTypeSelectionStr.equals("item"))
				{
					Item item = (Item)editor.getBuildObjects().get("item");
					
					if(specificSelectionStr.equals("ID"))
					{
						String specificValue = String.valueOf(item.getId());
						tfSpecific.setText(specificValue);
						
						return;
					}
				}

				tfSpecific.setTouchable(Touchable.disabled);
			}
		});
		
		
		tfSpecific.setTextFieldListener(new TextFieldListener()
		{
			@Override
			public void keyTyped(TextField textField, char key)
			{				
				String buildObjectTypeSelectionStr = sbBuildObjectType.getSelection();
				String specificSelectionStr = sbSpecific.getSelection();
				
				if(buildObjectTypeSelectionStr.equals("block"))
				{
					Block block = (Block)editor.getBuildObjects().get("block");
					
					if(specificSelectionStr.equals("Collidable"))
					{
						String specificValue = tfSpecific.getText();
						boolean isCollidable = Utils.stringToBool(specificValue);
						block.setCollidable(isCollidable);
					}
					else if(specificSelectionStr.equals("Trap"))
					{
						String specificValue = tfSpecific.getText();
						boolean isTrap = Utils.stringToBool(specificValue);
						block.setTrap(isTrap);
					}
					else if(specificSelectionStr.equals("Ladder"))
					{
						String specificValue = tfSpecific.getText();
						boolean isLadder = Utils.stringToBool(specificValue);
						block.setLadder(isLadder);
					}
				}
				else if(buildObjectTypeSelectionStr.equals("enemy"))
				{
					Enemy enemy = (Enemy)editor.getBuildObjects().get("enemy");
					
					if(specificSelectionStr.equals("Facing left"))
					{
						String specificValue = tfSpecific.getText();
						boolean isFacingLeft = Utils.stringToBool(specificValue);
						enemy.setFacingLeft(isFacingLeft);
						
						Sprite spr = buildObjectSprites.get("enemy");
						
						if(enemy.isFacingLeft())
						{
							spr.setAnimation("idleLeft");
						}
						else
						{
							spr.setAnimation("idleRight");
						}
						
						spObjectImage.setWidget(new Image(spr.getCurrentFrame()));
					}
				}
				else if(buildObjectTypeSelectionStr.equals("cannon"))
				{
					Cannon cannon = (Cannon)editor.getBuildObjects().get("cannon");
					
					if(specificSelectionStr.equals("Shot delay"))
					{
						String specificValue = tfSpecific.getText();
						
						try
						{
							long shotDelay = Long.parseLong(specificValue);
							cannon.setShotDelay(shotDelay);
						}
						catch(Exception e)
						{
							
						}
					}
				}
				else if(buildObjectTypeSelectionStr.equals("item"))
				{
					Item item = (Item)editor.getBuildObjects().get("item");
					
					if(specificSelectionStr.equals("ID"))
					{
						String specificValue = tfSpecific.getText();
						
						try
						{
							int id = Integer.parseInt(specificValue);
							item.setId(id);
						}
						catch(Exception e)
						{
							
						}
					}
				}
			}
		});

		////
		
		stage.addActor(window);
		
		// block
		sbBuildObjectType.setSelection(1);
	}
	
	public void render()
	{
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	@Override
	public boolean keyDown(int keycode)
	{
		return stage.keyDown(keycode);
	}

	@Override
	public boolean keyUp(int keycode)
	{
		return stage.keyUp(keycode);
	}

	@Override
	public boolean keyTyped(char character)
	{
		return stage.keyTyped(character);
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		return stage.touchDown(screenX, screenY, pointer, button);
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		return stage.touchUp(screenX, screenY, pointer, button);
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		return stage.touchDragged(screenX, screenY, pointer);
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		return stage.mouseMoved(screenX, screenY);
	}

	@Override
	public boolean scrolled(int amount)
	{
		return stage.scrolled(amount);
	}
}
