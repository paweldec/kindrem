package com.pde.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.pde.game.Blood.BloodType;
import com.pde.game.Editor.ActionType;
import com.pde.game.GameObject.GameObjectType;
import com.pde.game.GameObject.State;
import com.pde.game.Item.ItemType;
import com.pde.game.Level.Layer;
import com.pde.game.Utils.Direction;

public class WorldRenderer
{
	// FOR EDITOR
	private Sprite editorEnemySprite = SpriteManager.getSprite("enemy");
	private Sprite editorBlockSprite = SpriteManager.getSprite("block");
	private Sprite editorCannonSprite = SpriteManager.getSprite("cannon");
	private Sprite editorItemSprite = SpriteManager.getSprite("item");
	// END EDITOR
	
	// HUD
	private Sprite heartSprite = SpriteManager.getSprite("item");
	private Sprite touchPanelSprite = SpriteManager.getSprite("touch_panel");
	//
	
	private int cameraWidth;
	private int cameraHeight;
	
	private static final int MAX_BLOOD_SPRITES = 50;
	
	private World world;
	Editor editor;
	private OrthographicCamera camera;
	
	private boolean lightOn;
	
	// shape renderer
	private ShapeRenderer shapeRenderer = new ShapeRenderer();
	
	// textures
	private final Sprite bobSprite;
	private final Sprite blockSprite;
	private final Vector<Sprite> enemySprites;
	private final Vector<Sprite> cannonSprites;
	private final Vector<Sprite> bloodSprites;
	private final Vector<Sprite> itemSprites;
	private final Sprite shotSprite;
	
	
	private SpriteBatch spriteBatch;
	private SpriteBatch HUDBatch;
	private int width;
	private int height;
	private float ppuX; // pixels per unit on the X axis
	private float ppuY; // -||-
	
	private Rectangle _mapBorderRect;
	private Rectangle _clearPointerRect;
	
	public WorldRenderer(World world)
	{
		this.world = world;
		editor = world.getEditor();
		
		lightOn = true;
		
		setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		spriteBatch = new SpriteBatch();
		HUDBatch = new SpriteBatch();
		HUDBatch.setProjectionMatrix(camera.combined);
		
		bobSprite = SpriteManager.getSprite("player");
		blockSprite = SpriteManager.getSprite("block");
		shotSprite = SpriteManager.getSprite("fireshot");
		enemySprites = new Vector<Sprite>();
		cannonSprites = new Vector<Sprite>();
		bloodSprites = new Vector<Sprite>();
		itemSprites = new Vector<Sprite>();		
		
		for(int i = 0; i < world.getEnemies().size(); i++)
		{
			enemySprites.add(SpriteManager.getSprite("enemy"));
		}
		
		for(int i = 0; i < world.getCannons().size(); i++)
		{
			cannonSprites.add(SpriteManager.getSprite("cannon"));
		}
		
		for(int i = 0; i <= MAX_BLOOD_SPRITES; i++)
		{
			bloodSprites.add(SpriteManager.getSprite("blood"));
		}
		
		_mapBorderRect = new Rectangle();
		_clearPointerRect = new Rectangle();
	}
	
	public void setSize(int w, int h)
	{
		this.width = w;
		this.height = h;
		
		this.cameraWidth = (int) (w / Settings.WIDTH_UNIT_SIZE);
		this.cameraHeight = (int) (h / Settings.HEIGHT_UNIT_SIZE);
		
		if(camera == null)
		{
			this.camera = new OrthographicCamera(cameraWidth, cameraHeight); // TODO: kamere zrobic jako model
			this.camera.position.set(5.0f, 3.5f, 0.0f);
			this.camera.update();
		}
		
		ppuX = 1.0f;
		ppuY = 1.0f;
	}
	
	private void drawShots()
	{
		Direction direction;
		
		for(Shot shot : world.getShots())
		{		 
			direction = shot.getDirection();
			
			if(direction.equals(Direction.LEFT))
			{
				shotSprite.setAnimation("left");
			}
			else if(direction.equals(Direction.RIGHT))
			{
				shotSprite.setAnimation("right");
			}
			else if(direction.equals(Direction.UP))
			{
				shotSprite.setAnimation("up");
			}
			else if(direction.equals(Direction.DOWN))
			{
				shotSprite.setAnimation("down");
			}
			
			spriteBatch.draw(shotSprite.getCurrentFrame(), shot.getPosition().x * ppuX, shot.getPosition().y * ppuY, shot.getSizeX() * ppuX, shot.getSizeY() * ppuY);
		}
	}
	
	private void drawBlood()
	{		
		for(Blood blood : world.getBloods())
		{
			int id = blood.getId();
			
			if(blood.getBloodType().equals(BloodType.SHOT))
			{
				Direction direction = blood.getDirection();
				
				if(direction.equals(Direction.RIGHT))
				{
					bloodSprites.get(id).setAnimation("shotRight");
				}
				else if(direction.equals(Direction.LEFT))
				{
					bloodSprites.get(id).setAnimation("shotLeft");
				}
			}
			
			spriteBatch.draw(bloodSprites.get(id).getCurrentFrame(), blood.getPosition().x * ppuX, blood.getPosition().y * ppuY, blood.getSizeX() * ppuX, blood.getSizeY() * ppuY);
		}
	}
	
	private void drawBlockBackground() 
	{	
		for (Block block : world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight, Layer.BACKGROUND)) 
		{
			lightBegin(block);
			
			blockSprite.setAnimation("" + block.getType());
			spriteBatch.draw(blockSprite.getCurrentFrame(), block.getPosition().x * ppuX, block.getPosition().y * ppuY, block.getSizeX() * ppuX, block.getSizeY() * ppuY);
		
			lightEnd();
		}
		
		for (Block block : world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight, Layer.LEVEL)) 
		{
			lightBegin(block);
			
			blockSprite.setAnimation("" + block.getType());
			spriteBatch.draw(blockSprite.getCurrentFrame(), block.getPosition().x * ppuX, block.getPosition().y * ppuY, block.getSizeX() * ppuX, block.getSizeY() * ppuY);
			
			lightEnd();
		}
	}
	
	private void drawForeground()
	{
		for (Block block : world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight, Layer.FOREGROUND)) 
		{
			lightBegin(block);
			
			blockSprite.setAnimation("" + block.getType());
			spriteBatch.draw(blockSprite.getCurrentFrame(), block.getPosition().x * ppuX, block.getPosition().y * ppuY, block.getSizeX() * ppuX, block.getSizeY() * ppuY);
		
			lightEnd();
		}
	}
	
	private void drawItems()
	{
		// for editor
		int itemNum = world.getItems().size();
		int spritesNum = itemSprites.size();		
		
		if(spritesNum < itemNum)
		{
			for(int i = 0; i < (itemNum - spritesNum); i++)
			{
				itemSprites.add(SpriteManager.getSprite("item"));
			}
		}
		// end for editor
		
		int itemIndex = 0;
		
		for(Item item : world.getItems())
		{
			switch(item.getItemType())
			{
				case ITEM_COIN:
				{	
					itemSprites.get(itemIndex).setAnimation("item_coin");
					break;
				}
				case ITEM_KEY_GREEN:
				{
					itemSprites.get(itemIndex).setAnimation("item_key_green");
					break;
				}
				case ITEM_KEY_BROWN:
				{
					itemSprites.get(itemIndex).setAnimation("item_key_brown");
					break;
				}
				case ITEM_KEY_SILVER:
				{
					itemSprites.get(itemIndex).setAnimation("item_key_silver");
					break;
				}
				case ITEM_KEY_GOLD:
				{
					itemSprites.get(itemIndex).setAnimation("item_key_gold");
					break;
				}
				case ITEM_DOOR_GREEN:
				{
					if(item.isDoorOpen())
					{
						itemSprites.get(itemIndex).setAnimation("item_door_green_opened");
					}
					else
					{
						itemSprites.get(itemIndex).setAnimation("item_door_green_closed");
					}
					
					break;
				}
				case ITEM_DOOR_BROWN:
				{
					if(item.isDoorOpen())
					{
						itemSprites.get(itemIndex).setAnimation("item_door_brown_opened");
					}
					else
					{
						itemSprites.get(itemIndex).setAnimation("item_door_brown_closed");
					}
					
					break;
				}
				case ITEM_DOOR_SILVER:
				{
					if(item.isDoorOpen())
					{
						itemSprites.get(itemIndex).setAnimation("item_door_silver_opened");
					}
					else
					{
						itemSprites.get(itemIndex).setAnimation("item_door_silver_closed");
					}
					
					break;
				}
				case ITEM_DOOR_GOLD:
				{
					if(item.isDoorOpen())
					{
						itemSprites.get(itemIndex).setAnimation("item_door_gold_opened");
					}
					else
					{
						itemSprites.get(itemIndex).setAnimation("item_door_gold_closed");
					}
					
					break;
				}
				case ITEM_HEART:
				{
					itemSprites.get(itemIndex).setAnimation("item_heart_red");
					break;
				}
			}
			
			lightBegin(item);
			
			spriteBatch.draw(itemSprites.get(itemIndex).getCurrentFrame(), item.getPosition().x * ppuX, item.getPosition().y * ppuY, item.getSizeX() * ppuX, item.getSizeY() * ppuY);
		
			lightEnd();
			
			itemIndex++;
		}
	}
	
	private void drawCannons()
	{
		// for editor
		int cannonNum = world.getCannons().size();
		int spritesNum = cannonSprites.size();		
		
		if(spritesNum < cannonNum)
		{
			for(int i = 0; i < (cannonNum - spritesNum); i++)
			{
				cannonSprites.add(SpriteManager.getSprite("cannon"));
			}
		}
		// end for editor
		
		Direction direction;
		
		int cannonIndex = 0;
		
		for (Cannon cannon : world.getCannons()) 
		{
			direction = cannon.getDirection();
			
			if(direction.equals(Direction.LEFT))
			{
				cannonSprites.get(cannonIndex).setAnimation("idleLeft");
			}
			else if(direction.equals(Direction.RIGHT))
			{
				cannonSprites.get(cannonIndex).setAnimation("idleRight");
			}
			else if(direction.equals(Direction.UP))
			{
				cannonSprites.get(cannonIndex).setAnimation("idleUp");
			}
			else
			{
				cannonSprites.get(cannonIndex).setAnimation("idleDown");
			}
			
			lightBegin(cannon);
			
			spriteBatch.draw(cannonSprites.get(cannonIndex).getCurrentFrame(), 
					cannon.getPosition().x * ppuX, cannon.getPosition().y * ppuY, cannon.getSizeX() * ppuX, 
					cannon.getSizeY() * ppuY);
			
			lightEnd();
			
			cannonIndex++;
		}
	}
	
	/*
	private void drawPlatforms()
	{
		for(Platform platform : world.getPlatforms())
		{
			spriteBatch.draw(platformTexture, platform.getPosition().x * ppuX, platform.getPosition().y * ppuY, platform.getSizeX() * ppuX, platform.getSizeY() * ppuY);
		}
	}
	*/
	
	private void drawBob() 
	{
		Bob bob = world.getBob();
		
		if(bob.getState().equals(State.IDLE))
		{
			if(bob.isFacingLeft())
			{
				bobSprite.setAnimation("idleLeft");
			}
			else
			{
				bobSprite.setAnimation("idleRight");
			}	
		}
		else if(bob.getState().equals(State.WALKING)) 
		{
			if(bob.isFacingLeft())
			{
				bobSprite.setAnimation("walkLeft");
			}
			else
			{
				bobSprite.setAnimation("walkRight");
			}
		} 
		else if (bob.getState().equals(State.JUMPING)) 
		{
			if (bob.getVelocity().y > 0) 
			{
				// up
				if(bob.isFacingLeft())
				{
					bobSprite.setAnimation("idleLeft");
				}
				else
				{
					bobSprite.setAnimation("idleRight");
				}
			} 
			else 
			{
				// fall down
				if(bob.isFacingLeft())
				{
					bobSprite.setAnimation("idleLeft");
				}
				else
				{
					bobSprite.setAnimation("idleRight");
				}
			}
		}

		spriteBatch.draw(bobSprite.getCurrentFrame(), bob.getPosition().x * ppuX, bob.getPosition().y * ppuY, bob.getSizeX() * ppuX, bob.getSizeY() * ppuY);
	}
	
	private void drawHUD()
	{
		int life = world.getBob().getLife();
		int maxLife = world.getBob().getMaxLife();
		
		for(int i = 0; i < maxLife; i++)
		{
			if(i < life)
			{
				heartSprite.setAnimation("item_heart_red");
			}
			else
			{
				heartSprite.setAnimation("item_heart_silver");
			}
			
			HUDBatch.draw(heartSprite.getCurrentFrame(), 0.3f + (0.6f * i) * ppuX, 6.3f * ppuY, 0.5f * ppuX, 0.4f * ppuY);
		}
		
		if(Settings.TOUCH_CONTROL_PANEL)
		{
			TouchPanel touchPanel = world.getTouchPanel();
			touchPanelSprite.setAnimation("left");
			
			HUDBatch.draw(touchPanelSprite.getCurrentFrame(), touchPanel.getLeftTouchArea().x * ppuX, touchPanel.getLeftTouchArea().y * ppuY, 
					touchPanel.getLeftTouchArea().width * ppuX, touchPanel.getLeftTouchArea().height * ppuY);
		
			touchPanelSprite.setAnimation("right");
			
			HUDBatch.draw(touchPanelSprite.getCurrentFrame(), touchPanel.getRightTouchArea().x * ppuX, touchPanel.getRightTouchArea().y * ppuY, 
					touchPanel.getRightTouchArea().width * ppuX, touchPanel.getRightTouchArea().height * ppuY);
		
			touchPanelSprite.setAnimation("a");
			
			HUDBatch.draw(touchPanelSprite.getCurrentFrame(), touchPanel.getaTouchArea().x * ppuX, touchPanel.getaTouchArea().y * ppuY, 
					touchPanel.getaTouchArea().width * ppuX, touchPanel.getaTouchArea().height * ppuY);
		
			touchPanelSprite.setAnimation("b");
			
			HUDBatch.draw(touchPanelSprite.getCurrentFrame(), touchPanel.getbTouchArea().x * ppuX, touchPanel.getbTouchArea().y * ppuY, 
					touchPanel.getbTouchArea().width * ppuX, touchPanel.getbTouchArea().height * ppuY);
		}
	}
	
	private void drawEnemies()
	{	
		// for editor
		int enemyNum = world.getEnemies().size();
		int spritesNum = enemySprites.size();		
		
		if(spritesNum < enemyNum)
		{
			for(int i = 0; i < (enemyNum - spritesNum); i++)
			{
				enemySprites.add(SpriteManager.getSprite("enemy"));
			}
		}
		// end for editor
		
		
		int enemyIndex = 0;
		
		for(Enemy enemy : world.getEnemies())
		{
			if(enemy.getState().equals(State.IDLE))
			{
				if(enemy.isFacingLeft())
				{
					enemySprites.get(enemyIndex).setAnimation("idleLeft");
				}
				else
				{
					enemySprites.get(enemyIndex).setAnimation("idleRight");
				}
			}
			else if(enemy.getState().equals(State.WALKING)) 
			{
				if(enemy.isFacingLeft())
				{
					enemySprites.get(enemyIndex).setAnimation("walkLeft");
				}
				else
				{
					enemySprites.get(enemyIndex).setAnimation("walkRight");
				}
			} 
			else if (enemy.getState().equals(State.JUMPING)) 
			{
				if (enemy.getVelocity().y > 0) 
				{
					if(enemy.isFacingLeft())
					{
						enemySprites.get(enemyIndex).setAnimation("idleLeft");
					}
					else
					{
						enemySprites.get(enemyIndex).setAnimation("idleRight");
					}
				} 
				else 
				{
					if(enemy.isFacingLeft())
					{
						enemySprites.get(enemyIndex).setAnimation("idleLeft");
					}
					else
					{
						enemySprites.get(enemyIndex).setAnimation("idleRight");
					}
				}
			}
			
			lightBegin(enemy);

			spriteBatch.draw(enemySprites.get(enemyIndex).getCurrentFrame(), enemy.getPosition().x * ppuX, enemy.getPosition().y * ppuY, enemy.getSizeX() * ppuX, enemy.getSizeY() * ppuY);
			
			lightEnd();
			
			enemyIndex++;
		}
		
	}
	
	private void drawMapBorder()
	{
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Line);
	
		_mapBorderRect.x = 0;
		_mapBorderRect.y = 0;
		_mapBorderRect.width = world.getLevel().getWidth();
		_mapBorderRect.height = world.getLevel().getHeight();
		
		shapeRenderer.setColor(new Color(1.0f, 0.0f, 0.0f, 1.0f));
		shapeRenderer.rect(_mapBorderRect.x, _mapBorderRect.y, _mapBorderRect.width, _mapBorderRect.height);
		
		shapeRenderer.end();		
	}
	
	private void drawDebug()
	{
		// render blocks
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Line);
		
		Rectangle rect;
		
		for(Block block : world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight, Layer.BACKGROUND))
		{
			rect = block.getBounds();
			
			shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
			shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}
		
		for(Block block : world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight, Layer.LEVEL))
		{
			rect = block.getBounds();
			
			if(block.isCollidable())
			{
				shapeRenderer.setColor(new Color(1.0f, 0.0f, 0.0f, 1.0f));
			}
			else
			{
				shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
			}
			
			shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}
		
		for(Block block : world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight, Layer.FOREGROUND))
		{
			rect = block.getBounds();
			
			shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
			shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}
		
		// render Bob
		Bob bob = world.getBob();
		rect = bob.getBounds();
		
		shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
		shapeRenderer.rect(rect.x,  rect.y, rect.width, rect.height);
		
		// render enemies
		for(Enemy enemy : world.getEnemies())
		{
			rect = enemy.getBounds();
			shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
			shapeRenderer.rect(rect.x,  rect.y, rect.width, rect.height);
		}
		
		// render cannons
		for(Cannon cannon : world.getCannons())
		{
			rect = cannon.getBounds();
			shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
			shapeRenderer.rect(rect.x,  rect.y, rect.width, rect.height);
		}
		
		// render platforms
		for(Platform platform : world.getPlatforms())
		{
			rect = platform.getBounds();
			shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
			shapeRenderer.rect(rect.x,  rect.y, rect.width, rect.height);
		}
		
		// render items
		for(Item item : world.getItems())
		{
			rect = item.getBounds();
			shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
			shapeRenderer.rect(rect.x,  rect.y, rect.width, rect.height);
		}
		
		// render bloods
		for(Blood blood : world.getBloods())
		{
			rect = blood.getBounds();
			shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
			shapeRenderer.rect(rect.x,  rect.y, rect.width, rect.height);
		}
		
		// render shoots
		for(Shot shot : world.getShots())
		{
			rect = shot.getBounds();
			shapeRenderer.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));
			shapeRenderer.rect(rect.x,  rect.y, rect.width, rect.height);
		}
		
		shapeRenderer.end();		
	}
	
	private void drawCollisionBlocks()
	{
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
		
		for(Rectangle rect : world.getCollisionRects())
		{
			shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}
		
		shapeRenderer.end();
	}
	
	private void drawEditorClearMode()
	{
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Line);
	
		_clearPointerRect.x = editor.getCurrentBuildPosition().x;
		_clearPointerRect.y = editor.getCurrentBuildPosition().y;
		_clearPointerRect.width = 1.0f;
		_clearPointerRect.height = 1.0f;
		
		shapeRenderer.setColor(new Color(1.0f, 0.0f, 0.0f, 1.0f));
		shapeRenderer.rect(_clearPointerRect.x, _clearPointerRect.y, _clearPointerRect.width, _clearPointerRect.height);
		
		shapeRenderer.end();	
	}

	private void drawEditorBuildMode()
	{
		Editor editor = world.getEditor();
		TextureRegion textureRegion = null;
		float sizeX = 0;
		float sizeY = 0;
		
		if(editor.getBuildObjectType().equals(GameObjectType.BLOCK))
		{
			Block block = (Block)editor.getBuildObjects().get("block");
			
			editorBlockSprite.setAnimation("" + block.getType());
			
			textureRegion = editorBlockSprite.getCurrentFrame();
			sizeX = block.getSizeX();
			sizeY = block.getSizeY();
		}
		else if(editor.getBuildObjectType().equals(GameObjectType.ENEMY))
		{
			Enemy enemy = (Enemy)editor.getBuildObjects().get("enemy");
			
			if(enemy.isFacingLeft())
			{
				editorEnemySprite.setAnimation("idleLeft");
			}
			else
			{
				editorEnemySprite.setAnimation("idleRight");
			}
			
			textureRegion = editorEnemySprite.getCurrentFrame();
			sizeX = enemy.getSizeX();
			sizeY = enemy.getSizeY();
		}
		else if(editor.getBuildObjectType().equals(GameObjectType.CANNON))
		{
			Cannon cannon = (Cannon)editor.getBuildObjects().get("cannon");
			
			if(cannon.getDirection().equals(Direction.UP))
			{
				editorCannonSprite.setAnimation("idleUp");
			}
			else if(cannon.getDirection().equals(Direction.LEFT))
			{
				editorCannonSprite.setAnimation("idleLeft");
			}
			else if(cannon.getDirection().equals(Direction.RIGHT))
			{
				editorCannonSprite.setAnimation("idleRight");
			}
			else if(cannon.getDirection().equals(Direction.DOWN))
			{
				editorCannonSprite.setAnimation("idleDown");
			}
			
			textureRegion = editorCannonSprite.getCurrentFrame();

			sizeX = cannon.getSizeX();
			sizeY = cannon.getSizeY();
		}
		else if(editor.getBuildObjectType().equals(GameObjectType.ITEM))
		{
			Item editorItem = (Item)editor.getBuildObjects().get("item");
			
			switch(editorItem.getItemType())
			{
				case ITEM_COIN:
				{
					editorItemSprite.setAnimation("item_coin");
					break;
				}
				case ITEM_HEART:
				{
					editorItemSprite.setAnimation("item_heart_red");
					break;
				}
				case ITEM_KEY_GREEN:
				{
					editorItemSprite.setAnimation("item_key_green");
					break;
				}
				case ITEM_KEY_BROWN:
				{
					editorItemSprite.setAnimation("item_key_brown");
					break;
				}
				case ITEM_KEY_SILVER:
				{
					editorItemSprite.setAnimation("item_key_silver");
					break;
				}
				case ITEM_KEY_GOLD:
				{
					editorItemSprite.setAnimation("item_key_gold");
					break;
				}
				case ITEM_DOOR_GREEN:
				{
					editorItemSprite.setAnimation("item_door_green_closed");
					break;
				}
				case ITEM_DOOR_BROWN:
				{
					editorItemSprite.setAnimation("item_door_brown_closed");
					break;
				}
				case ITEM_DOOR_SILVER:
				{
					editorItemSprite.setAnimation("item_door_silver_closed");
					break;
				}
				case ITEM_DOOR_GOLD:
				{
					editorItemSprite.setAnimation("item_door_gold_closed");
					break;
				}
			}
			
			textureRegion = editorItemSprite.getCurrentFrame();
			
			sizeX = editorItem.getSizeX();
			sizeY = editorItem.getSizeY();
		}
		
		spriteBatch.draw(textureRegion, editor.getCurrentBuildPosition().x * ppuX, editor.getCurrentBuildPosition().y * ppuY, sizeX * ppuX, sizeY * ppuY);
	}
	
	private float calcLight(GameObject lightObject, GameObject object)
	{
		double distance = Utils.calcDistanceBetweenPoints(lightObject.getPosition().x, 
				lightObject.getPosition().y, object.getPosition().x, object.getPosition().y);
		
		if(distance < 4.0f)
		{
			return 1.0f - (((float)(distance * 25.0)) / 100.0f);
		}
			
		return 0.0f;
	}
/*
	private void updateLights()
	{
		Bob bob = world.getBob();
		LinkedList<Shot> shots = world.getShots();
		List<Block> backgroundBlocks = world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight, Layer.BACKGROUND);
		List<Block> levelBlocks = world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight, Layer.LEVEL);
		List<Block> foregroundBlocks = world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight, Layer.FOREGROUND);
		List<Cannon> cannons = world.getCannons();
		List<Enemy> enemies = world.getEnemies();
		
		int lightUpdateCount = 0;
		
		for(Block block : backgroundBlocks)
		{
			block.setLight(0.0f);
			
			float light = calcLight(world.getBob(), block);
			
			block.addLight(light);
			
			for(Shot shot : shots)
			{
				light = calcLight(shot, block);
				
				block.addLight(light);
				
				lightUpdateCount++;
			}
			
			lightUpdateCount++;
		}
		
		for(Block block : levelBlocks)
		{
			block.setLight(0.0f);
			
			float light = calcLight(world.getBob(), block);
			
			block.addLight(light);
			
			for(Shot shot : shots)
			{
				light = calcLight(shot, block);
				
				block.addLight(light);
				
				lightUpdateCount++;
			}
			
			lightUpdateCount++;
		}
		
		for(Block block : foregroundBlocks)
		{
			block.setLight(0.0f);
			
			float light = calcLight(world.getBob(), block);
			
			block.addLight(light);
			
			for(Shot shot : shots)
			{
				light = calcLight(shot, block);
				block.addLight(light);
				
				lightUpdateCount++;
			}
			
			lightUpdateCount++;
		}
		
		for(Cannon cannon : cannons)
		{
			cannon.setLight(0.0f);
			
			float light = calcLight(world.getBob(), cannon);
			
			cannon.addLight(light);
			
			for(Shot shot : shots)
			{
				light = calcLight(shot, cannon);
				cannon.addLight(light);
				
				lightUpdateCount++;
			}
			
			lightUpdateCount++;
		}
		
		for(Enemy enemy : enemies)
		{
			enemy.setLight(0.0f);
			
			float light = calcLight(world.getBob(), enemy);
			
			enemy.addLight(light);
			
			for(Shot shot : shots)
			{
				light = calcLight(shot, enemy);
				enemy.addLight(light);
				
				lightUpdateCount++;
			}
			
			lightUpdateCount++;
		}
		
		System.out.println("update light count: " + lightUpdateCount);
	}
*/	
	public void render()
	{		
		if(Settings.EDITOR_MODE)
		{
			lightOn = false;
		}
		else
		{
			lightOn = true;
			//updateLights();
		}
		
		
		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.begin();
			drawBlockBackground();
			//drawPlatforms();
			drawCannons();
			drawItems();
			drawEnemies();
			drawBlood();
			drawBob();
			drawShots();
			drawForeground();
			
			if(Settings.EDITOR_MODE)
			{
				if(editor.getActionType().equals(ActionType.BUILD))
				{
					drawEditorBuildMode();
				}
			}
			
		spriteBatch.end();		
		
		if(Settings.DEBUG_MODE)
		{
			drawCollisionBlocks();
			drawDebug();
		}
		
		if(Settings.EDITOR_MODE)
		{
			if(editor.getActionType().equals(ActionType.CLEAR))
			{
				drawEditorClearMode();
			}
		}
		
		if(Settings.EDITOR_MODE || Settings.DEBUG_MODE)
		{
			drawMapBorder();
		}

		if(!Settings.EDITOR_MODE)
		{
			HUDBatch.begin();
			drawHUD();
			HUDBatch.end();
		}
	}
	
	public OrthographicCamera getCamera()
	{
		return camera;
	}
	
	private void lightBegin(GameObject gameObject)
	{
		if(lightOn)
		{
			float light = calcLight(world.getBob(), gameObject);
			spriteBatch.setColor(light, light, light, 1);
		}
		else
		{
			spriteBatch.setColor(1, 1, 1, 1);
		}
	}
	
	private void lightEnd()
	{
		if(lightOn)
		{
			spriteBatch.setColor(1, 1, 1, 1);
		}
	}
}
