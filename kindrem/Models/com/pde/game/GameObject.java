package com.pde.game;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class GameObject
{	
	public enum GameObjectType
	{
		PLAYER,
		ENEMY,
		SHOT,
		CANNON,
		PLATFORM,
		ITEM,
		BLOCK,
		BLOOD,
		EDITOR,
		TOUCH_PANEL
	}
	
	public enum State
	{
		IDLE, 
		WALKING, 
		JUMPING, 
		DYING,
		HANG,
		LADDER
	}
	
	private GameObjectType type;
	private ControllerBase controller = null;
	
	protected Vector2 position = new Vector2();
	protected Rectangle bounds = new Rectangle();
	protected Vector2 acceleration = new Vector2();
	protected Vector2 velocity = new Vector2();
	
	protected State state = State.IDLE;
	protected float stateTime = 0;
	
	protected boolean facingLeft;
	protected boolean startFacingLeft;

	protected float sizeX;
	protected float sizeY;
	
	private final float startPosX;
	private final float startPosY;
	
	private float light = 1.0f;
	
	protected GameObject(Vector2 position, float sizeX, float sizeY, GameObjectType type)
	{
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.position = position;
		this.bounds.x = position.x;
		this.bounds.y = position.y;
		this.bounds.height = sizeY;
		this.bounds.width = sizeX;
		this.type = type;
		this.startPosX = position.x;
		this.startPosY = position.y;
		this.facingLeft = true;
		this.startFacingLeft = facingLeft;
	}
	
	public void setController(ControllerBase controller)
	{
		this.controller = controller;
	}
	
	public ControllerBase getController()
	{
		return controller;
	}
	
	public void setSize(float sizeX, float sizeY)
	{
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.bounds.x = position.x;
		this.bounds.y = position.y;
		this.bounds.height = sizeY;
		this.bounds.width = sizeX;
	}
	
	public float getSizeX()
	{
		return sizeX;
	}
	
	public float getSizeY()
	{
		return sizeY;
	}
	
	public GameObjectType getGameObjectType()
	{
		return type;
	}
	
	public boolean isFacingLeft()
	{
		return facingLeft;
	}
	
	public void setFacingLeft(boolean facingLeft)
	{
		this.facingLeft = facingLeft;
	}
	
	public Vector2 getPosition()
	{
		return position;
	}
	
	public Vector2 getAcceleration()
	{
		return acceleration;
	}
	
	public Vector2 getVelocity()
	{
		return velocity;
	}
	
	public Rectangle getBounds()
	{
		return bounds;
	}
	
	public State getState()
	{
		return state;
	}
	
	public void setState(State newState)
	{
		this.state = newState;
	}
	
	public float getStateTime()
	{
		return stateTime;
	}
	
	public void setPosition(Vector2 position)
	{
		this.position = position;
		this.bounds.setX(position.x);
		this.bounds.setY(position.y);
	}
	
	public void setPosition(float x, float y)
	{
		this.position.x = x;
		this.position.y = y;
		this.bounds.setX(x);
		this.bounds.setY(y);
	}
	
	public void setAcceleration(Vector2 acceleration)
	{
		this.acceleration = acceleration;
	}
	
	public void setVelocity(Vector2 velocity)
	{
		this.velocity = velocity;
	}
	
	public void setBounds(Rectangle bounds)
	{
		this.bounds = bounds;
	}
	
	public void setStateTime(float stateTime)
	{
		this.stateTime = stateTime;
	}
	
	public void update(float delta)
	{
		stateTime += delta;
	}
	
	public float getStartPosX()
	{
		return startPosX;
	}
	
	public float getStartPosY()
	{
		return startPosY;
	}
	
	public boolean isStartFacingLeft()
	{
		return startFacingLeft;
	}
	
	public float getLight()
	{
		return light;
	}
/*	
	public void setLight(float light)
	{
		this.light = light;
	}
	
	public void addLight(float light)
	{
		if((this.light + light) < 1.0f)
		{
			this.light += light;
		}
		else
		{
			light = 1.0f;
		}
	}
*/
}
