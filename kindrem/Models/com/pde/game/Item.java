package com.pde.game;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.Vector2;

public class Item extends GameObject
{
	enum ItemType
	{
		ITEM_COIN,
		ITEM_KEY_GREEN,
		ITEM_KEY_BROWN,
		ITEM_KEY_SILVER,
		ITEM_KEY_GOLD,
		ITEM_DOOR_GREEN,
		ITEM_DOOR_BROWN,
		ITEM_DOOR_SILVER,
		ITEM_DOOR_GOLD,
		ITEM_HEART,
	};
	
	private ItemType itemType;	
	private int id;
	private boolean doorOpen;
	//private Map<String, Integer> specificValue;
	
	protected Item(Vector2 position, ItemType itemType)
	{
		super(position, 1.0f, 1.0f, GameObjectType.ITEM);
		
		setItemType(itemType);		
		this.id = -1;
		this.doorOpen = false;
		//specificValue = new HashMap<String, Integer>();
	}
	/*
	public void setSpecificValue(String key, int value)
	{
		specificValue.put(key, value);
	}
	
	public Integer getSpecificValue(String key)
	{
		if(specificValue.containsKey(key))
		{
			return specificValue.get(key);
		}
		
		return null;
	}
	*/
	public ItemType getItemType()
	{
		return itemType;
	}
	
	public void setItemType(ItemType type)
	{
		if(type.equals(ItemType.ITEM_COIN))
		{
			this.setSize(0.3f, 0.5f);
		}
		else if(type.equals(ItemType.ITEM_KEY_GREEN) || 
				type.equals(ItemType.ITEM_KEY_BROWN) ||
				type.equals(ItemType.ITEM_KEY_SILVER) ||
				type.equals(ItemType.ITEM_KEY_GOLD))
		{
			this.setSize(0.3f, 0.5f);
		}
		else if(type.equals(ItemType.ITEM_DOOR_GREEN) ||
				type.equals(ItemType.ITEM_DOOR_BROWN) ||
				type.equals(ItemType.ITEM_DOOR_SILVER) || 
				type.equals(ItemType.ITEM_DOOR_GOLD))
		{
			this.setSize(0.2f, 1.0f);
		}
		else if(type.equals(ItemType.ITEM_HEART))
		{
			this.setSize(0.5f, 0.4f);
		}
		
		itemType = type;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public boolean isDoorOpen()
	{
		return doorOpen;
	}
	
	public void setDoorOpened(boolean value)
	{
		this.doorOpen = value;
	}
}
