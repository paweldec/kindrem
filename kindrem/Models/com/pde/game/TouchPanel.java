package com.pde.game;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class TouchPanel extends GameObject
{
	private Rectangle leftTouchArea;
	private Rectangle rightTouchArea;
	private Rectangle aTouchArea;
	private Rectangle bTouchArea;
	
	protected TouchPanel()
	{
		super(new Vector2(0.0f, 0.0f), 0.1f, 0.1f, GameObjectType.TOUCH_PANEL);
		
		leftTouchArea = new Rectangle();
		rightTouchArea = new Rectangle();
		aTouchArea = new Rectangle();
		bTouchArea = new Rectangle();
		
		leftTouchArea.x = 0.5f;
		leftTouchArea.y = 0.5f;
		leftTouchArea.width = 1.0f;
		leftTouchArea.height = 1.0f;
		
		rightTouchArea.x = 2.0f;
		rightTouchArea.y = 0.5f;
		rightTouchArea.width = 1.0f;
		rightTouchArea.height = 1.0f;
		
		aTouchArea.x = 7.0f;
		aTouchArea.y = 0.5f;
		aTouchArea.width = 1.0f;
		aTouchArea.height = 1.0f;
		
		bTouchArea.x = 8.5f;
		bTouchArea.y = 0.5f;
		bTouchArea.width = 1.0f;
		bTouchArea.height = 1.0f;
	}

	public Rectangle getLeftTouchArea()
	{
		return leftTouchArea;
	}

	public void setLeftTouchArea(Rectangle leftTouchArea)
	{
		this.leftTouchArea = leftTouchArea;
	}

	public Rectangle getRightTouchArea()
	{
		return rightTouchArea;
	}

	public void setRightTouchArea(Rectangle rightTouchArea)
	{
		this.rightTouchArea = rightTouchArea;
	}

	public Rectangle getaTouchArea()
	{
		return aTouchArea;
	}

	public void setaTouchArea(Rectangle aTouchArea)
	{
		this.aTouchArea = aTouchArea;
	}

	public Rectangle getbTouchArea()
	{
		return bTouchArea;
	}

	public void setbTouchArea(Rectangle bTouchArea)
	{
		this.bTouchArea = bTouchArea;
	}
	
}
