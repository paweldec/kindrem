package com.pde.game;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.pde.game.Item.ItemType;
import com.pde.game.Level.Layer;
import com.pde.game.Utils.Direction;

public class Editor extends GameObject
{
	public enum ActionType
	{
		BUILD,
		CLEAR
	}
	
	public enum PrecisionType
	{
		NORMAL,
		BLOCK
	}
	
	//int blockType;
	//boolean enemyFacingLeft;
	//Direction cannonDirection;
	
	private ActionType actionType;
	private GameObjectType buildObjectType;
	private PrecisionType precisionType;
	private Vector2 currentBuildPosition;
	
	private Layer currentLayer;
	
	private Map<String, GameObject> buildObjects;
	
	protected Editor()
	{
		super(new Vector2(0, 0), Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), GameObjectType.EDITOR);
		
		actionType = ActionType.BUILD;
		buildObjectType = GameObjectType.BLOCK;
		precisionType = PrecisionType.BLOCK;
		
		currentBuildPosition = new Vector2(0.0f, 0.0f);
		
		//blockType = 0;
		//enemyFacingLeft = false;
		//cannonDirection = Direction.RIGHT;
		
		currentLayer = Layer.LEVEL;
		
		buildObjects = new HashMap<String, GameObject>();
		buildObjects.put("block", new Block(new Vector2(0, 0), 0));
		buildObjects.put("enemy", new Enemy(new Vector2(0, 0), false));
		buildObjects.put("cannon", new Cannon(new Vector2(0, 0), Direction.RIGHT));
		buildObjects.put("item", new Item(new Vector2(0, 0), ItemType.ITEM_COIN));
	}
	
	public ActionType getActionType()
	{
		return actionType;
	}
	
	public void setActionType(ActionType type)
	{
		actionType = type;
	}
	
	public GameObjectType getBuildObjectType()
	{
		return buildObjectType;
	}
	
	public void setBuildObjectType(GameObjectType type)
	{
		buildObjectType = type;
	}
	
	public Vector2 getCurrentBuildPosition()
	{
		return currentBuildPosition;
	}
	
	public void setCurrentBuildPosition(float x, float y)
	{
		currentBuildPosition.x = x;
		currentBuildPosition.y = y;
	}
	
	public PrecisionType getPrecisionType()
	{
		return precisionType;
	}
	
	public void setPrecisionType(PrecisionType type)
	{
		precisionType = type;
	}
	
	public Map<String, GameObject> getBuildObjects()
	{
		return buildObjects;
	}
	
	public Layer getCurrentLayer()
	{
		return currentLayer;
	}
	
	public void setCurrentLayer(Layer layer)
	{
		System.out.println("Setting current layer to: " + layer);
		currentLayer = layer;
	}
	
	/*
	public int getBlockType()
	{
		return blockType;
	}
	
	public void setBlockType(int type)
	{
		blockType = type;
	}
	
	public boolean getEnemyFacingLeft()
	{
		return enemyFacingLeft;
	}
	
	public void setEnemyFacingLeft(boolean value)
	{
		enemyFacingLeft = value;
	}
	
	public Direction getCannonDirection()
	{
		return cannonDirection;
	}
	
	public void setCannonDirection(Direction direction)
	{
		cannonDirection = direction;
	}
	*/
}
