package com.pde.game;

import java.util.Vector;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.pde.game.Item.ItemType;

public class Bob extends GameObject
{		
	private int life;
	private int maxLife;
	private Vector<Item> gainedKeys;
	
	public Bob(Vector2 position)
	{
		super(position, 0.5f, 0.5f, GameObjectType.PLAYER);
		
		gainedKeys = new Vector<Item>();
		life = maxLife = 3;
	}
	
	public int getLife()
	{
		return life;
	}
	
	public void setLife(int life)
	{
		this.life = life;
	}
	
	public int getMaxLife()
	{
		return maxLife;
	}
	
	public void setMaxLife(int maxLife)
	{
		this.maxLife = maxLife;
	}
	
	public void gainKey(Item item)
	{
		if(item.getItemType().equals(ItemType.ITEM_KEY_GREEN) || 
		   item.getItemType().equals(ItemType.ITEM_KEY_BROWN) ||
		   item.getItemType().equals(ItemType.ITEM_KEY_SILVER) ||
		   item.getItemType().equals(ItemType.ITEM_KEY_GOLD))
		{
			System.out.println("key gained");
			
			gainedKeys.add(item);
		}
	}
	
	public boolean tryOpenDoor(Item door)
	{
		if(!door.getItemType().equals(ItemType.ITEM_DOOR_GREEN) &&
		   !door.getItemType().equals(ItemType.ITEM_DOOR_BROWN) && 
		   !door.getItemType().equals(ItemType.ITEM_DOOR_SILVER) &&
		   !door.getItemType().equals(ItemType.ITEM_DOOR_GOLD))
		{
			return false;
		}
		
		for(int i = 0; i < gainedKeys.size(); i++)
		{
			if(gainedKeys.get(i).getId() == door.getId())
			{
				gainedKeys.remove(i);
				return true;
			}
		}
		
		return false;
	}
}
