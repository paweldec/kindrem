package com.pde.game;

import com.badlogic.gdx.math.Vector2;
import com.pde.game.GameObject.GameObjectType;
import com.pde.game.Utils.Direction;

public class Cannon extends GameObject
{
	private static int cannonCounter = 0;
	private Direction direction;
	private long shotDelay;
	
	protected Cannon(Vector2 position, Direction direction)
	{
		super(position, 1.0f, 1.0f, GameObjectType.CANNON);	
		cannonCounter++;
		shotDelay = 2000;
		
		setDirection(direction);
	}
	
	public void setDirection(Direction direction)
	{
		this.direction = direction;
	}
	
	public Direction getDirection()
	{
		return direction;
	}
	
	public static int getCannonCount()
	{
		return cannonCounter;
	}
	
	public long getShotDelay()
	{
		return shotDelay;
	}
	
	public void setShotDelay(long shotDelay)
	{
		this.shotDelay = shotDelay;
	}
}
