package com.pde.game;

import com.badlogic.gdx.math.Vector2;

public class Enemy extends GameObject
{	
	private static int enemyCounter = 0;
	private int life = 100;
	
	protected Enemy(Vector2 position, boolean facingLeft)
	{
		super(position, 0.5f, 0.5f, GameObjectType.ENEMY);
		enemyCounter++;
		
		this.facingLeft = facingLeft;
		this.startFacingLeft = facingLeft;
	}
	
	public int getLife()
	{
		return life;
	}
	
	public void setLife(int life)
	{
		this.life = life;
	}
	
	public static int getEnemyCount()
	{
		return enemyCounter;
	}
}
