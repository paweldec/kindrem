package com.pde.game;

import com.badlogic.gdx.math.Vector2;
import com.pde.game.Utils.Direction;

public class Blood extends GameObject
{
	enum BloodType
	{
		SHOT,
	}
	private static int lastId = 0;
	private int id = 0;
	
	private BloodType bloodType;
	private Direction direction;
	private long startTimeStamp;

	protected Blood(Vector2 position, BloodType bloodType, Direction direction)
	{
		super(position, 1.0f, 1.0f, GameObjectType.BLOOD);
		
		this.bloodType = bloodType;
		this.direction = direction;
		this.startTimeStamp = System.currentTimeMillis();
		
		id = lastId;
		
		if(lastId < 50)
		{
			lastId++;
		}
		else
		{
			lastId = 0;
		}
	}
	
	public Direction getDirection()
	{
		return direction;
	}
	
	public BloodType getBloodType()
	{
		return bloodType;
	}
	
	public long getStartTimeStamp()
	{
		return startTimeStamp;
	}
	
	public int getId()
	{
		return id;
	}
}
