package com.pde.game;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Block extends GameObject
{	
	int type;
	boolean collidable;
	boolean ladder;
	boolean trap;
	
	public Block(Vector2 pos, int type)
	{
		super(pos, 1.0f, 1.0f, GameObjectType.BLOCK);
		this.type = type;
		
		type = 0;
		collidable = true;
		trap = false;
		ladder = false;
	}
	
	public boolean isLadder()
	{
		return ladder;
	}
	
	public void setLadder(boolean ladder)
	{				
		this.ladder = ladder;
	}
	
	public boolean isCollidable()
	{
		return collidable;
	}

	public void setCollidable(boolean collidable)
	{
		this.collidable = collidable;
	}

	public boolean isTrap()
	{
		return trap;
	}

	public void setTrap(boolean trap)
	{
		this.trap = trap;
	}

	public int getType()
	{
		return type;
	}
	
	public void setType(int type)
	{
		this.type = type;
	}
}
