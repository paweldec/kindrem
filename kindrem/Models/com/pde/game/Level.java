package com.pde.game;

import com.badlogic.gdx.math.Vector2;

public class Level
{
	enum Layer
	{
		BACKGROUND,
		LEVEL,
		FOREGROUND
	}

	private String name;
	private int width;
	private int height;
	private Block[][] blocks;
	private Block[][] background;
	private Block[][] foreground;
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public Block get(int col, int row, Layer layer)
	{
		if(!isCoord(col, row))
		{
			return null;
		}
		
		if(layer.equals(Layer.LEVEL))
		{
			return blocks[col][row];
		}
		else if(layer.equals(Layer.BACKGROUND))
		{
			return background[col][row];
		}
		else
		{
			return foreground[col][row];
		}
	}
	
	public void setBlocks(Block[][] blocks)
	{
		this.blocks = blocks;
	}
	
	public Block[][] getBlocks()
	{
		return blocks;
	}
	
	public Block[][] getBackground()
	{
		return background;
	}
	
	public Block[][] getForeground()
	{
		return foreground;
	}
	
	public Level()
	{
		loadDemoLevel2();
	}

	private void loadDemoLevel()
	{
		width = 10;
		height = 7;
		blocks = new Block[width][height];
		
		clear();
		
		for (int col = 0; col < 10; col++) 
		{
			blocks[col][0] = new Block(new Vector2(col, 0), 0);
			blocks[col][6] = new Block(new Vector2(col, 6), 0);
		
			if (col > 2) 
			{
				blocks[col][1] = new Block(new Vector2(col, 1), 0);
			}
		}
		
		blocks[9][2] = new Block(new Vector2(9, 2), 0);
		blocks[9][3] = new Block(new Vector2(9, 3), 0);
		blocks[9][4] = new Block(new Vector2(9, 4), 0);
		blocks[9][5] = new Block(new Vector2(9, 5), 0);
		
		blocks[6][3] = new Block(new Vector2(6, 3), 0);
		blocks[6][4] = new Block(new Vector2(6, 4), 0);
		blocks[6][5] = new Block(new Vector2(6, 5), 0);
	}
	
	private void loadDemoLevel2()
	{
		width = 20;
		height = 20;
		blocks = new Block[width][height];
		background = new Block[width][height];
		foreground = new Block[width][height];
		
		clear();
		
		/*
		addBlock(0, 0, 0);
		addBlock(0, 1, 0);
		addBlock(0, 2, 0);
		addBlock(0, 3, 0);
		addBlock(0, 4, 0);
		*/
		addBlock(1, 0, 0, Layer.LEVEL);
		/*
		addBlock(2, 0, 0);
		addBlock(3, 0, 0);
		addBlock(4, 0, 0);
		addBlock(5, 0, 0);
		addBlock(6, 0, 0);
		addBlock(7, 0, 0);
		addBlock(8, 0, 0);
		addBlock(9, 0, 0);
		addBlock(10, 0, 0);
		addBlock(11, 0, 0);
		addBlock(12, 0, 0);
		addBlock(13, 0, 0);
		addBlock(14, 0, 0);
		addBlock(15, 0, 0);
		addBlock(16, 0, 0);
		addBlock(17, 0, 0);
		addBlock(18, 0, 0);
		addBlock(19, 0, 0);	
		*/
		/*
		addBlock(19, 0, 0);
		addBlock(19, 1, 0);
		addBlock(19, 2, 0);
		addBlock(19, 3, 0);
		addBlock(19, 4, 0);
		*/
		//
	}
	
	public void addBlock(int col, int row, int type, Layer layer)
	{
		if(!isCoord(col, row))
		{
			return;
		}
		
		if(layer.equals(Layer.LEVEL))
		{
			blocks[col][row] = new Block(new Vector2(col, row), type);
		}
		else if(layer.equals(Layer.BACKGROUND))
		{
			background[col][row] = new Block(new Vector2(col, row), type);
		}
		else if(layer.equals(Layer.FOREGROUND))
		{
			foreground[col][row] = new Block(new Vector2(col, row), type);
		}
		
	}
	
	public void addBlock(int col, int row, Block block, Layer layer)
	{
		if(!isCoord(col, row))
		{
			return;
		}
		
		if(layer.equals(Layer.LEVEL))
		{
			blocks[col][row] = block;
		}
		else if(layer.equals(Layer.BACKGROUND))
		{
			background[col][row] = block;
		}
		else if(layer.equals(Layer.FOREGROUND))
		{
			foreground[col][row] = block;
		}
	}
	
	public void removeBlock(int col, int row, Layer layer)
	{
		if(!isCoord(col, row))
		{
			return;
		}
		
		if(layer.equals(Layer.LEVEL))
		{
			blocks[col][row] = null;
		}
		else if(layer.equals(Layer.BACKGROUND))
		{
			background[col][row] = null;
		}
		else if(layer.equals(Layer.FOREGROUND))
		{
			foreground[col][row] = null;
		}
	}
	
	private boolean isCoord(int col, int row)
	{
		if(col < 0 || row < 0 || col >= width || row >= height)
		{
			//System.out.println("Level cord is out of map!");
			return false;
		}
		
		return true;
	}
	
	public void clear()
	{
		for (int col = 0; col < width; col++) 
		{
			for (int row = 0; row < height; row++) 
			{
				blocks[col][row] = null;
				background[col][row] = null;
				foreground[col][row] = null;
			}
		}
	}
	
	public void changeSize(int width, int height)
	{
		Block[][] newBlocks = new Block[width][height];
		Block[][] newBackground = new Block[width][height];
		Block[][] newForeground = new Block[width][height];
		
		for (int col = 0; col < width; col++) 
		{
			for (int row = 0; row < height; row++) 
			{
				if(isCoord(col, row))
				{
					newBlocks[col][row] = blocks[col][row];
					newBackground[col][row] = background[col][row];
					newForeground[col][row] = foreground[col][row];
				}
				else
				{
					newBlocks[col][row] = null;
					newBackground[col][row] = null;
					newForeground[col][row] = null;
				}
			}
		}
		
		this.width = width;
		this.height = height;
		blocks = newBlocks;
		background = newBackground;
		foreground = newForeground;
	}
}
