package com.pde.game;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.pde.game.GameObject.GameObjectType;
import com.pde.game.Utils.Direction;

public class Platform extends GameObject
{
	private float changeDirectionDelay;
	private int currentPoint;
	private ArrayList<Vector2> points;
	private boolean forwardDirection;
	
	protected Platform(ArrayList<Vector2> points, float changeDirectionDelay)
	{
		super(points.get(0), 3.0f, 0.5f, GameObjectType.PLATFORM);
		
		this.changeDirectionDelay = changeDirectionDelay;
		this.currentPoint = 0;
		this.points = points;
		this.forwardDirection = true;
	}
	
	public int getCurrentPoint()
	{
		return currentPoint;
	}
	
	public void setCurrentPoint(int currentPoint)
	{
		if(currentPoint < points.size())
		{
			this.currentPoint = currentPoint;
		}
	}
	
	public int getMaxPoint()
	{
		return points.size() - 1;
	}
	
	public ArrayList<Vector2> getPoints()
	{
		return points;
	}
	
	public boolean isForwardDirection()
	{
		return forwardDirection;
	}
}
