package com.pde.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.pde.game.Blood.BloodType;
import com.pde.game.GameObject.GameObjectType;
import com.pde.game.Level.Layer;
import com.pde.game.Utils.Direction;

public class World
{
	private Editor editor;
	private TouchPanel touchPanel;
	
	private Bob bob;
	private ArrayList<Enemy> enemies = new ArrayList<Enemy>();
	private Level level;
	private LinkedList<Shot> shots = new LinkedList<Shot>();
	private LinkedList<Blood> bloods = new LinkedList<Blood>();
	private ArrayList<Cannon> cannons = new ArrayList<Cannon>();
	private ArrayList<Platform> platforms = new ArrayList<Platform>();
	private ArrayList<Item> items = new ArrayList<Item>();
	
	private Array<Rectangle> collisionRects = new Array<Rectangle>();
	
	public Array<Rectangle> getCollisionRects()
	{
		return collisionRects;
	}
	
	public Bob getBob()
	{
		return bob;
	}
	
	public Editor getEditor()
	{
		return editor;
	}
	
	public TouchPanel getTouchPanel()
	{
		return touchPanel;
	}
	
	public ArrayList<Enemy> getEnemies()
	{
		return enemies;
	}
	
	public Level getLevel()
	{
		return level;
	}
	
	public ArrayList<Cannon> getCannons()
	{
		return cannons;
	}
	
	public ArrayList<Platform> getPlatforms()
	{
		return platforms;
	}
	
	public ArrayList<Item> getItems()
	{
		return items;
	}
	
	public World()
	{
		createDemoWorld();
	}
	
	private void createDemoWorld()
	{
		editor = new Editor();
		
		if(Settings.TOUCH_CONTROL_PANEL)
		{
			touchPanel = new TouchPanel();
		}
		
		bob = new Bob(new Vector2(1, 1));
		level = new Level();
	}
	
	public List<Block> getDrawableBlocks(int width, int height, Layer layer)
	{
		int x = (int)bob.getPosition().x - width;
		int y = (int)bob.getPosition().y - height;
		
		if(x < 0)
		{
			x = 0;
		}
		
		if(y < 0)
		{
			y = 0;
		}
		
		int x2 = x + 2 * width;
		int y2 = y + 2 * height;
		
		if(x2 > level.getWidth())
		{
			x2 = level.getWidth() - 1;
		}
		
		if(y2 > level.getHeight())
		{
			y2 = level.getHeight() - 1;
		}
		
		List<Block> blocks = new ArrayList<Block>();
		Block block;
		
		for(int col = x; col <= x2; col++)
		{
			for(int row = y; row <= y2; row++)
			{
				block = level.get(col, row, layer);

				if(block != null)
				{
					blocks.add(block);
				}
			}
		}
		
		return blocks;
	}
	
	public LinkedList<Shot> getShots()
	{
		return shots;
	}
	
	public void addShot(float startPosX, float startPosY, Direction direction, GameObject gameObject)
	{		
		shots.add(new Shot(new Vector2(startPosX, startPosY), direction, gameObject));
	}
	
	public LinkedList<Blood> getBloods()
	{
		return bloods;
	}
	
	public void addBlood(float startPosX, float startPosY, Direction direction, BloodType bloodType)
	{
		bloods.add(new Blood(new Vector2(startPosX, startPosY), bloodType, direction));
	}
	
	public void addBlock(int x, int y, int type, Layer layer)
	{
		level.addBlock(x, y, type, layer);
	}
	
	public void addBlock(int x, int y, Block block, Layer layer)
	{
		level.addBlock(x, y, block, layer);
	}
	
	public void removeBlock(int x, int y, Layer layer)
	{
		level.removeBlock(x, y, layer);
	}
	
	public void addEnemy(Enemy enemy)
	{
		EnemyController enemyController = new EnemyController(this, enemy);
		
		enemies.add(enemy);
		ControllerBase.addController("enemyController" + Enemy.getEnemyCount(), enemyController);
	}
	
	public void removeEnemy(Rectangle rect)
	{		
		for(Enemy enemy : enemies)
		{
			if(rect.contains(enemy.getBounds()))
			{
				ControllerBase controller = enemy.getController();
				ControllerBase.removeController(controller.getName());
				enemies.remove(enemy);
				
				return;
			}
		}
	}
	
	public void addCannon(Cannon cannon)
	{
		CannonController cannonController = new CannonController(this, cannon);
		
		cannons.add(cannon);
		ControllerBase.addController("cannonController" + Cannon.getCannonCount(), cannonController);
	}
	
	public void removeCannon(Rectangle rect)
	{
		for(Cannon cannon : cannons)
		{
			if(rect.contains(cannon.getBounds()))
			{
				ControllerBase controller = cannon.getController();
				ControllerBase.removeController(controller.getName());
				cannons.remove(cannon);
				
				return;
			}
		}
	}
	
	public void addItem(Item item)
	{
		if(!ControllerBase.getAllControllers().containsKey("itemController"))
		{
			ItemController itemController = new ItemController(this);
			ControllerBase.addController("itemController", itemController);
		}
		
		items.add(item);
	}
	
	public void removeItem(Rectangle rect)
	{
		for(Item item : items)
		{
			if(rect.contains(item.getBounds()))
			{
				items.remove(item);
				
				if(items.size() <= 0)
				{
					ControllerBase.removeController("itemController");
				}
				
				return;
			}
		}
	}
	
	public void clear()
	{
		level.clear();	
		
		for(int i = 0; i < enemies.size(); i++)
		{
			ControllerBase.removeController(enemies.get(i).getController().getName());
		}
		
		enemies.clear();
		
		for(int i = 0; i < cannons.size(); i++)
		{
			ControllerBase.removeController(cannons.get(i).getController().getName());
		}
		
		cannons.clear();
	}
}
