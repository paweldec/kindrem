package com.pde.game;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.pde.game.Utils.Direction;

public class Shot extends GameObject
{
	private float speed = 5.0f;
	private Direction direction;
	private GameObject gameObject;
	
	private static int lastId = 0;
	private int id = 0;
	
	public static final float SIZEX = 0.5f;
	public static final float SIZEY = 0.3f;
	
	public Shot(Vector2 position, Direction direction, GameObject gameObject)
	{		
		super(position, SIZEX, SIZEY, GameObjectType.SHOT);
		
		id = lastId;
		
		if(lastId < 50)
		{
			lastId++;
		}
		else
		{
			lastId = 0;
		}
			
		if(direction.equals(Direction.LEFT))
		{
			position.x = position.x - SIZEX;
			position.y = position.y - (SIZEY / 2.0f);
			
			this.setPosition(position);
		}
		else if(direction.equals(Direction.RIGHT))
		{
			position.x = position.x;
			position.y = position.y - (SIZEY / 2.0f);
			
			this.setPosition(position);
		}
		else if(direction.equals(Direction.UP))
		{
			//tmpPosX = startPosX - (Shot.SIZEX / 2.0f);
			//tmpPosY = startPosY - Shot.SIZEY;
			
			this.setSize(SIZEY, SIZEX);
		}
		else if(direction.equals(Direction.DOWN))
		{
			//tmpPosX = startPosX - (Shot.SIZEX / 2.0f);
			
			this.setSize(SIZEY, SIZEX);
		}
		
		this.gameObject = gameObject;
		this.direction = direction;
	}
	
	public GameObject getOwner()
	{
		return gameObject;
	}
	
	public float getSpeed()
	{
		return speed;
	}
	
	public Direction getDirection()
	{
		return direction;
	}
	
	public int getId()
	{
		return id;
	}
}
