package com.pde.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.pde.game.Level.Layer;

public abstract class ControllerBase
{
	private static Map<String, ControllerBase> controllers = new ConcurrentHashMap<String, ControllerBase>();
	
	protected World world;
	private Array<Block> collidable = new Array<Block>();
	private String name;
	
	protected ControllerBase(World world)
	{
		this.world = world;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public static void addController(String name, ControllerBase controller)
	{
		controller.setName(name);
		controllers.put(name, controller);
	}
	
	public static void removeController(String name)
	{
		if(controllers.containsKey(name))
		{
			controllers.remove(name);
		}
	}
	
	public static ControllerBase getController(String name)
	{
		return controllers.get(name);
	}
	
	public static  Map<String, ControllerBase> getAllControllers()
	{
		return controllers;
	}
	
	public abstract void update(float delta);
	
	public boolean keyDownEvent(int keycode)
	{
		return false;
	}

	public boolean keyUpEvent(int keycode)
	{		
		return false;
	}

	public boolean keyTypedEvent(char character)
	{
		return false;
	}

	public boolean touchDownEvent(int screenX, int screenY, int screenWidth, int screenHeight, int pointer, int button)
	{
		return false;
	}

	public boolean touchUpEvent(int screenX, int screenY, int screenWidth, int screenHeight, int pointer, int button)
	{
		return false;
	}

	public boolean touchDraggedEvent(int screenX, int screenY, int pointer)
	{
		return false;
	}

	public boolean mouseMovedEvent(int screenX, int screenY)
	{
		return false;
	}

	public boolean scrolledEvent(int amount)
	{
		return false;
	}
	
	public boolean mapCollisionLeftEvent(GameObject gameObject1, GameObject gameObject2)
	{
		return false;
	}
	
	public boolean mapCollisionRightEvent(GameObject gameObject1, GameObject gameObject2)
	{
		return false;
	}
	
	public boolean mapCollisionUpEvent(GameObject gameObject1, GameObject gameObject2)
	{
		return false;
	}
	
	public boolean mapCollisionDownEvent(GameObject gameObject1, GameObject gameObject2)
	{
		return false;
	}
	
	protected boolean updateMapCollisionEvents(GameObject gameObject, float delta) 
	{
		boolean collisionOccured = false;
		
		// scale velocity to frame units 
		gameObject.getVelocity().mul(delta);
		
		// Obtain the rectangle from the pool instead of instantiating it
		Rectangle creatureRect = new Rectangle();
		
		// set the rectangle to bob's bounding box
		creatureRect.set(gameObject.getBounds().x, gameObject.getBounds().y, gameObject.getBounds().width, gameObject.getBounds().height);
		
		// we first check the movement on the horizontal X axis
		int startX, endX;
		int startY = (int) gameObject.getBounds().y;
		int endY = (int) (gameObject.getBounds().y + gameObject.getBounds().height);
		
		// if Bob is heading left then we check if he collides with the block on his left
		// we check the block on his right otherwise
		if (gameObject.getVelocity().x < 0) 
		{
			startX = endX = (int) Math.floor(gameObject.getBounds().x + gameObject.getVelocity().x);
		} 
		else 
		{
			startX = endX = (int) Math.floor(gameObject.getBounds().x + gameObject.getBounds().width + gameObject.getVelocity().x);
		}

		// get the block(s) bob can collide with
		populateCollidableBlocks(startX, startY, endX, endY);

		// simulate bob's movement on the X
		creatureRect.x += gameObject.getVelocity().x;
		
		// clear collision boxes in world
		world.getCollisionRects().clear();
		
		// if bob collides, make his horizontal velocity 0
		for (Block block : collidable) 
		{
			if (block == null) continue;
			
			if (creatureRect.overlaps(block.getBounds())) 
			{
				if(gameObject.getVelocity().x < 0.0f)
				{
					collisionOccured = true;
					mapCollisionLeftEvent(gameObject, block);
				}
				else
				{
					collisionOccured = true;
					mapCollisionRightEvent(gameObject, block);
				}
				
				world.getCollisionRects().add(block.getBounds());
				break;
			}
		}

		// reset the x position of the collision box
		creatureRect.x = gameObject.getPosition().x;
		
		// the same thing but on the vertical Y axis
		startX = (int) gameObject.getBounds().x;
		endX = (int) (gameObject.getBounds().x + gameObject.getBounds().width);
		
		if (gameObject.getVelocity().y < 0) 
		{
			startY = endY = (int) Math.floor(gameObject.getBounds().y + gameObject.getVelocity().y);
		} 
		else 
		{
			startY = endY = (int) Math.floor(gameObject.getBounds().y + gameObject.getBounds().height + gameObject.getVelocity().y);
		}
		
		populateCollidableBlocks(startX, startY, endX, endY);
		
		creatureRect.y += gameObject.getVelocity().y;
		
		for (Block block : collidable) 
		{
			if (block == null) continue;
			
			if (creatureRect.overlaps(block.getBounds())) 
			{
				if (gameObject.getVelocity().y < 0) 
				{
					collisionOccured = true;
					mapCollisionDownEvent(gameObject, block);
				}
				else
				{
					collisionOccured = true;
					mapCollisionUpEvent(gameObject, block);
				}
				
				world.getCollisionRects().add(block.getBounds());
				break;
			}
		}
		
		// reset the collision box's position on Y
		creatureRect.y = gameObject.getPosition().y;
		
		/////////////
		
		creatureRect.x += gameObject.getVelocity().x;
		
		for (Cannon cannon : world.getCannons()) 
		{			
			if (creatureRect.overlaps(cannon.getBounds())) 
			{
				if (gameObject.getVelocity().x < 0) 
				{
					collisionOccured = true;
					mapCollisionLeftEvent(gameObject, cannon);
				}
				else
				{
					collisionOccured = true;
					mapCollisionRightEvent(gameObject, cannon);
				}
				
				world.getCollisionRects().add(cannon.getBounds());
				break;
			}
		}
		
		creatureRect.x = gameObject.getPosition().x;
		
		creatureRect.y += gameObject.getVelocity().y;
		
		for (Cannon cannon : world.getCannons()) 
		{			
			if (creatureRect.overlaps(cannon.getBounds())) 
			{
				if (gameObject.getVelocity().y < 0) 
				{
					collisionOccured = true;
					mapCollisionDownEvent(gameObject, cannon);
				}
				else
				{
					collisionOccured = true;
					mapCollisionUpEvent(gameObject, cannon);
				}
				
				world.getCollisionRects().add(cannon.getBounds());
				break;
			}
		}
		
		/////////////
		
		// update creature position
		gameObject.getPosition().add(gameObject.getVelocity());
		gameObject.getBounds().x = gameObject.getPosition().x;
		gameObject.getBounds().y = gameObject.getPosition().y;
		
		// un-scale velocity (not in frame time)
		gameObject.getVelocity().mul(1 / delta);
		
		return collisionOccured;
	}
	
	protected void populateCollidableBlocks(int startX, int startY, int endX, int endY)
	{
		collidable.clear();

		for(int x = startX; x <= endX; x++)
		{
			for(int y = startY; y <= endY; y++)
			{
				if(x >= 0 && x < world.getLevel().getWidth() && y >= 0 && y < world.getLevel().getHeight())
				{
					Block block = world.getLevel().get(x, y, Layer.LEVEL);
					
					if(block != null && block.isCollidable())
					{
						collidable.add(world.getLevel().get(x, y, Layer.LEVEL));
					}
				}
			}
		}
	}
	
	protected boolean updateCreatureCollisionEvents(GameObject creature1, GameObject creature2)
	{		
		if(creature1.getBounds().overlaps(creature2.getBounds()))
		{
			creatureCollisionEvent(creature1, creature2);
			return true;
		}
		
		return false;
	}
	
	protected boolean updateCreatureCollisionEvents(GameObject creature1, ArrayList<? extends GameObject> creature2)
	{		 
		boolean collisionOccured = false;
		
		for (GameObject tmp : creature2) 
		{			
			if (creature1.getBounds().overlaps(tmp.getBounds())) 
			{				
				creatureCollisionEvent(creature1, tmp);
				collisionOccured = true;
			}
		}
		
		return collisionOccured;
	}
	
	protected boolean creatureCollisionEvent(GameObject creature1, GameObject creature2)
	{
		return false;
	}
}
