package com.pde.game;

import com.badlogic.gdx.math.Vector2;

public class PlatformController extends ControllerBase
{
	private Platform platform;
	//private Vector2 _vec = new Vector2(); // zmienna pomocnicza
	
	public PlatformController(World world, Platform platform)
	{
		super(world);
		this.platform = platform;
		this.platform.setController(this);
	}

	@Override
	public void update(float delta)
	{
		System.out.println("Platform position: " + platform.getPosition());
		System.out.println("Ide do punktu: " + platform.getPoints().get(platform.getCurrentPoint()));
		
		Vector2 point = platform.getPoints().get(platform.getCurrentPoint()); 
		Vector2 position = platform.getPosition();
		
		platform.getVelocity().x = (point.x - position.x);
		platform.getVelocity().y = (point.y - position.y);
		platform.getVelocity().nor();
		platform.getVelocity().mul(delta);
		
		position.x += platform.getVelocity().x;
		position.y += platform.getVelocity().y;
		
		platform.setPosition(position);
	
		System.out.println(Utils.calcDistanceBetweenPoints(position.x, position.y, point.x, point.y));
		
		if(Utils.calcDistanceBetweenPoints(position.x, position.y, point.x, point.y) < 0.1)
		{
			System.out.println("Zmiana kierunku");
			
			if(platform.getCurrentPoint() < platform.getMaxPoint())
			{
				platform.setCurrentPoint(platform.getCurrentPoint() + 1);
			}
			else
			{
				// TODO zrobic poruszanie w druga strone
				platform.setCurrentPoint(0);
			}
		}
		
		platform.update(delta);
	}
}
