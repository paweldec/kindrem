package com.pde.game;

import java.util.ArrayList;

import com.pde.game.GameObject.GameObjectType;
import com.pde.game.Item.ItemType;

public class ItemController extends ControllerBase
{	
	ArrayList<GameObject> itemsToRemove = new ArrayList<GameObject>();
	
	protected ItemController(World world)
	{
		super(world);
	}

	@Override
	public void update(float delta)
	{		
		updateCreatureCollisionEvents(world.getBob(), world.getItems());
		
		for(GameObject item : itemsToRemove)
		{
			world.getItems().remove(item);
		}
		
		itemsToRemove.clear();
	}	
	
	@Override
	public boolean creatureCollisionEvent(GameObject gameObject1, GameObject gameObject2)
	{		
		if(gameObject2.getGameObjectType().equals(GameObjectType.ITEM))
		{
			Item item = (Item)gameObject2;
			ItemType itemType = item.getItemType();
			
			if(itemType.equals(ItemType.ITEM_COIN) || itemType.equals(ItemType.ITEM_HEART))
			{		
				itemsToRemove.add(gameObject2);
				return true;
			}
			else if(itemType.equals(ItemType.ITEM_KEY_GREEN) ||
					itemType.equals(ItemType.ITEM_KEY_BROWN) ||
					itemType.equals(ItemType.ITEM_KEY_SILVER) ||
					itemType.equals(ItemType.ITEM_KEY_GOLD))
			{
				world.getBob().gainKey(item);
				
				itemsToRemove.add(gameObject2);
				
				return true;
			}
			else if(itemType.equals(ItemType.ITEM_DOOR_GREEN) || 
					itemType.equals(ItemType.ITEM_DOOR_BROWN) ||
					itemType.equals(ItemType.ITEM_DOOR_SILVER) ||
					itemType.equals(ItemType.ITEM_DOOR_GOLD))
			{
				world.getBob().getController().creatureCollisionEvent(gameObject1, gameObject2);
				return true;
			}
		}
		
		return false;
	}
}
