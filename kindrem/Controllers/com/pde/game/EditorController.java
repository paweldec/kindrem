package com.pde.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.pde.game.Editor.ActionType;
import com.pde.game.Editor.PrecisionType;
import com.pde.game.GameObject.GameObjectType;
import com.pde.game.Level.Layer;
import com.pde.game.Utils.Direction;

public class EditorController extends ControllerBase
{
	private Editor editor;
	private OrthographicCamera camera;
	
	private float _lastMousePosX = 0;
	private float _lastMousePosY = 0;
	
	protected EditorController(World world, Editor editor)
	{
		super(world);
		this.editor = editor;
		
		CameraController cameraController = (CameraController)ControllerBase.getController("cameraController");
		camera = cameraController.getCamera();
	}

	@Override
	public void update(float delta)
	{
		if(!Settings.EDITOR_MODE)
		{
			return;
		}
		
		camera.position.x = editor.getPosition().x;
		camera.position.y = editor.getPosition().y;
		
		editor.setCurrentBuildPosition(editor.getPosition().x + _lastMousePosX, editor.getPosition().y - _lastMousePosY);
		
		//System.out.println("Current build pos:" + editor.getCurrentBuildPosition());
	}
	
	@Override
	public boolean mouseMovedEvent(int screenX, int screenY)
	{
		if(!Settings.EDITOR_MODE)
		{
			return false;
		}
		
		// TODO poprawic obliczanie kursora (sa roznice w roznych rozmiarach okna)
		if(editor.getPrecisionType().equals(PrecisionType.NORMAL))
		{			
			_lastMousePosX = ((float)screenX / Settings.WIDTH_UNIT_SIZE) - 
					((Gdx.graphics.getWidth() / Settings.WIDTH_UNIT_SIZE) / 2.0f);
			_lastMousePosY = ((float)screenY / Settings.HEIGHT_UNIT_SIZE) - 
					((Gdx.graphics.getHeight() / Settings.HEIGHT_UNIT_SIZE) / 2.0f);
		}
		else if(editor.getPrecisionType().equals(PrecisionType.BLOCK))
		{
			_lastMousePosX = (int)(((screenX / Settings.WIDTH_UNIT_SIZE) - 
					(Gdx.graphics.getWidth() / Settings.WIDTH_UNIT_SIZE) / 2.0f));
			_lastMousePosY = (int)(((screenY / Settings.HEIGHT_UNIT_SIZE) - 
					(Gdx.graphics.getHeight() / Settings.HEIGHT_UNIT_SIZE) / 2.0f));
		}
		
		return true;
	}
	
	@Override
	public boolean keyDownEvent(int keycode)
	{
		if(keycode == Keys.E)
		{
			Settings.EDITOR_MODE = !Settings.EDITOR_MODE;
		}
		
		if(!Settings.EDITOR_MODE)
		{
			return false;
		}
		
		Vector2 position = editor.getPosition();
		
		if(keycode == Keys.W)
		{
			position.y++;
		}
		else if(keycode == Keys.S)
		{
			position.y--;
		}
		else if(keycode == Keys.A)
		{
			position.x--;
		}
		else if(keycode == Keys.D)
		{
			position.x++;
		}
		else if(keycode == Keys.NUM_1)
		{
			editor.setPrecisionType(PrecisionType.NORMAL);
		}
		else if(keycode == Keys.NUM_2)
		{
			editor.setPrecisionType(PrecisionType.BLOCK);
		}
		else if(keycode == Keys.F1)
		{
			editor.setBuildObjectType(GameObjectType.BLOCK);
		}
		else if(keycode == Keys.F2)
		{
			editor.setBuildObjectType(GameObjectType.ENEMY);
		}
		else if(keycode == Keys.F3)
		{
			editor.setBuildObjectType(GameObjectType.CANNON);
		}
		/*
		else if(keycode == Keys.O)
		{
			mapLoader.saveMap(Gdx.files.internal("maps/dev_map01.xml"));
		}
		else if(keycode == Keys.I)
		{
			mapLoader.loadMap(Gdx.files.internal("maps/dev_map01.xml"));
		}
		*/
		
		editor.setPosition(position);
		
		return true;
	}
	
	@Override	
	public boolean touchDownEvent(int screenX, int screenY, int screenWidth, int screenHeight, int pointer, int button)
	{
		if(!Settings.EDITOR_MODE)
		{
			return false;
		}
		
		if(editor.getActionType().equals(ActionType.BUILD))
		{
			if(editor.getBuildObjectType().equals(GameObjectType.BLOCK))
			{
				Block editorBlock = (Block)editor.getBuildObjects().get("block");
				Block newBlock = new Block(new Vector2((int)editor.getCurrentBuildPosition().x, (int)editor.getCurrentBuildPosition().y), editorBlock.getType());
				newBlock.setCollidable(editorBlock.isCollidable());
				newBlock.setTrap(editorBlock.isTrap());
				newBlock.setLadder(editorBlock.isLadder());
				
				world.addBlock((int)editor.getCurrentBuildPosition().x, (int)editor.getCurrentBuildPosition().y, newBlock, editor.getCurrentLayer());
			}
			else if(editor.getBuildObjectType().equals(GameObjectType.ENEMY))
			{
				Enemy editorEnemy = (Enemy)editor.getBuildObjects().get("enemy");
				Enemy newEnemy = new Enemy(new Vector2(editor.getCurrentBuildPosition().x, editor.getCurrentBuildPosition().y), editorEnemy.isFacingLeft());
				
				world.addEnemy(newEnemy);
			}
			else if(editor.getBuildObjectType().equals(GameObjectType.CANNON))
			{
				Cannon editorCannon = (Cannon)editor.getBuildObjects().get("cannon");
				Cannon newCannon = new Cannon(new Vector2((int)editor.getCurrentBuildPosition().x, (int)editor.getCurrentBuildPosition().y), editorCannon.getDirection());
				newCannon.setShotDelay(editorCannon.getShotDelay());
				
				world.addCannon(newCannon);
			}
			else if(editor.getBuildObjectType().equals(GameObjectType.ITEM))
			{
				Item editorItem = (Item)editor.getBuildObjects().get("item");
				Item newItem = new Item(new Vector2(editor.getCurrentBuildPosition().x, editor.getCurrentBuildPosition().y), editorItem.getItemType());
				newItem.setId(editorItem.getId());
				
				world.addItem(newItem);
			}
		}
		else if(editor.getActionType().equals(ActionType.CLEAR))
		{
			Rectangle rect = new Rectangle();
			
			if(editor.getPrecisionType().equals(PrecisionType.BLOCK))
			{
				rect.x = (int)editor.getCurrentBuildPosition().x;
				rect.y = (int)editor.getCurrentBuildPosition().y;
			}
			else
			{
				rect.x = editor.getCurrentBuildPosition().x;
				rect.y = editor.getCurrentBuildPosition().y;
			}
			
			rect.width = 1.0f;
			rect.height = 1.0f;
			
			if(editor.getBuildObjectType().equals(GameObjectType.BLOCK))
			{
				world.removeBlock((int)editor.getCurrentBuildPosition().x, (int)editor.getCurrentBuildPosition().y, editor.getCurrentLayer());
			}
			else if(editor.getBuildObjectType().equals(GameObjectType.ENEMY))
			{
				world.removeEnemy(rect);
			}
			else if(editor.getBuildObjectType().equals(GameObjectType.CANNON))
			{
				world.removeCannon(rect);
			}
			else if(editor.getBuildObjectType().equals(GameObjectType.ITEM))
			{
				world.removeItem(rect);
			}
		}

		return true;
	}
	/*
	@Override
	public boolean scrolledEvent(int amount)
	{
		if(!Settings.EDITOR_MODE)
		{
			return false;
		}
		
		if(editor.getBuildObjectType().equals(GameObjectType.BLOCK))
		{
			if(amount > 0)
			{
				if(editor.getBlockType() < 14)
				{
					editor.setBlockType(editor.getBlockType() + 1);
				}
				else
				{
					editor.setBlockType(0);
				}
			}
			else
			{
				if(editor.getBlockType() > 0)
				{
					editor.setBlockType(editor.getBlockType() - 1);
				}
				else
				{
					editor.setBlockType(13);
				}
			}
			
			return true;
		}
		else if(editor.getBuildObjectType().equals(GameObjectType.ENEMY))
		{
			editor.setEnemyFacingLeft(!editor.getEnemyFacingLeft());
			
			return true;
		}
		else if(editor.getBuildObjectType().equals(GameObjectType.CANNON))
		{
			if(amount > 0)
			{
				if(editor.getCannonDirection().equals(Direction.UP))
				{
					editor.setCannonDirection(Direction.RIGHT);
				}
				else if(editor.getCannonDirection().equals(Direction.RIGHT))
				{
					editor.setCannonDirection(Direction.DOWN);
				}
				else if(editor.getCannonDirection().equals(Direction.DOWN))
				{
					editor.setCannonDirection(Direction.LEFT);
				}
				else if(editor.getCannonDirection().equals(Direction.LEFT))
				{
					editor.setCannonDirection(Direction.UP);
				}
			}
			else
			{
				if(editor.getCannonDirection().equals(Direction.UP))
				{
					editor.setCannonDirection(Direction.LEFT);
				}
				else if(editor.getCannonDirection().equals(Direction.RIGHT))
				{
					editor.setCannonDirection(Direction.UP);
				}
				else if(editor.getCannonDirection().equals(Direction.DOWN))
				{
					editor.setCannonDirection(Direction.RIGHT);
				}
				else if(editor.getCannonDirection().equals(Direction.LEFT))
				{
					editor.setCannonDirection(Direction.DOWN);
				}
			}
			
			return true;
		}
		else if(editor.getBuildObjectType().equals(GameObjectType.ITEM_COIN))
		{
			
		}
		
		
		return false;
	} 
	*/

}
