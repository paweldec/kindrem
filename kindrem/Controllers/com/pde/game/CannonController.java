package com.pde.game;

import com.pde.game.Utils.Direction;

public class CannonController extends ControllerBase
{	
	private Cannon cannon;
	private long _lastTime = System.currentTimeMillis();
	
	public CannonController(World world, Cannon cannon)
	{
		super(world);
		this.cannon = cannon;
		this.cannon.setController(this);
	}

	@Override
	public void update(float delta)
	{
		if(_lastTime < (System.currentTimeMillis() - (cannon.getShotDelay())))
		{
			Direction direction = cannon.getDirection();
			
			if(direction.equals(Direction.LEFT))
			{
				world.addShot(cannon.getBounds().x + 0.5f, cannon.getBounds().y + (cannon.getBounds().height / 2.0f), cannon.getDirection(), cannon);
			}
			else if(direction.equals(Direction.RIGHT))
			{
				world.addShot((cannon.getBounds().x + cannon.getBounds().width - 0.5f), (cannon.getBounds().y + (cannon.getBounds().height / 2.0f)), cannon.getDirection(), cannon);
			}
			else if(direction.equals(Direction.UP))
			{
				world.addShot(cannon.getBounds().x + (cannon.getBounds().width / 2.0f) - 0.18f, cannon.getBounds().y + cannon.getBounds().height - 0.5f, cannon.getDirection(), cannon);
			}
			else
			{
				world.addShot(cannon.getBounds().x + (cannon.getBounds().width / 2.0f) - 0.1f, cannon.getBounds().y, cannon.getDirection(), cannon);
			}
			
			_lastTime = System.currentTimeMillis();
		}
	}
}
