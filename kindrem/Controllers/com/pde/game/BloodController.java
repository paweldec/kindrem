package com.pde.game;

import java.util.ArrayList;

import com.pde.game.Blood.BloodType;

public class BloodController extends ControllerBase
{
	private static final long SHOOT_TIME = 300l;
	
	private ArrayList<GameObject> bloodToRemove = new ArrayList<GameObject>();
	
	protected BloodController(World world)
	{
		super(world);
	}

	@Override
	public void update(float delta)
	{
		for(Blood blood : world.getBloods())
		{
			blood.update(delta);
			
			if(blood.getBloodType().equals(BloodType.SHOT))
			{
				if(blood.getStartTimeStamp() + SHOOT_TIME < System.currentTimeMillis())
				{
					bloodToRemove.add(blood);
				}
			}
		}
		
		for(GameObject blood : bloodToRemove)
		{
			world.getBloods().remove(blood);
		}
		
		bloodToRemove.clear();
	}
}
