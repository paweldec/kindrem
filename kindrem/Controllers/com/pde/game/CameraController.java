package com.pde.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class CameraController extends ControllerBase
{
	private Bob bob;
	private OrthographicCamera camera;
	
	private float lerp = 0.1f;
	
	public CameraController(World world, OrthographicCamera camera)
	{
		super(world);
		this.bob = world.getBob();
		this.camera = camera;
	}
	
	public void update(float delta)
	{
		Vector3 position = camera.position;
		
		if(!Settings.EDITOR_MODE)
		{
			position.x += (world.getBob().getPosition().x - position.x) * lerp;
			position.y += (world.getBob().getPosition().y - position.y) * lerp;
		}
		
		camera.position.set(position);		
		camera.update();
		
		
	}
	
	public OrthographicCamera getCamera()
	{
		return camera;
	}
}
