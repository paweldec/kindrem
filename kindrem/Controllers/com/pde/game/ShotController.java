package com.pde.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.math.Vector2;
import com.pde.game.GameObject.GameObjectType;
import com.pde.game.Utils.Direction;

public class ShotController extends ControllerBase
{	
	private static final double REMOVE_SHOT_DISTANCE = 6.0; 
	
	ArrayList<GameObject> shotsToRemove = new ArrayList<GameObject>();
	
	protected ShotController(World world)
	{
		super(world);
	}

	@Override
	public void update(float delta)
	{		
		//System.out.println("Total shots: " + world.getShots().size());
		
		final Iterator<Shot> i = world.getShots().iterator();
		Shot shot;
		Vector2 position;
		Direction direction;
		float speed;
		
		while (i.hasNext()) 
		{			
			shot = i.next();
			
			if(!updateMapCollisionEvents(shot, delta))
			{						
				// sprawdz i usun jesli shot jest daleko od postaci
				if(Utils.calcDistanceBetweenRects(world.getBob().getBounds(), shot.getBounds()) >= REMOVE_SHOT_DISTANCE)
				{
					shotsToRemove.add(shot);
				}
				else
				{
					updateCreatureCollisionEvents(shot, world.getBob());
					updateCreatureCollisionEvents(shot, world.getEnemies());
				}
			}
			
			position = shot.getPosition();
			direction = shot.getDirection();
			speed = shot.getSpeed();
			
			if(direction.equals(Direction.RIGHT))
			{
				position.x += (speed * delta);
			}
			else if(direction.equals(Direction.LEFT))
			{
				position.x -= (speed * delta);
			}
			else if(direction.equals(Direction.UP))
			{
				position.y += (speed * delta);
			}
			else if(direction.equals(Direction.DOWN))
			{
				position.y -= (speed * delta);
			}
		}
		
		for(GameObject shotToRemove : shotsToRemove)
		{
			world.getShots().remove(shotToRemove);
		}
		
		shotsToRemove.clear();
	}
	
	@Override
	public boolean mapCollisionLeftEvent(GameObject gameObject1, GameObject gameObject2)
	{
		if(gameObject2.getGameObjectType().equals(GameObjectType.CANNON))
		{
			if((GameObject)((Shot)gameObject1).getOwner() == gameObject2)
			{
				return false;
			}
		}
		
		shotsToRemove.add(gameObject1);
		
		return true;
	}
	
	@Override
	public boolean mapCollisionRightEvent(GameObject gameObject1, GameObject gameObject2)
	{		
		if(gameObject2.getGameObjectType().equals(GameObjectType.CANNON))
		{
			if((GameObject)((Shot)gameObject1).getOwner() == gameObject2)
			{
				return false;
			}
		}
		
		shotsToRemove.add(gameObject1);
		
		return true;
	}
	
	@Override
	public boolean mapCollisionUpEvent(GameObject gameObject1, GameObject gameObject2)
	{
		if(gameObject2.getGameObjectType().equals(GameObjectType.CANNON))
		{
			if((GameObject)((Shot)gameObject1).getOwner() == gameObject2)
			{
				return false;
			}
		}
		
		shotsToRemove.add(gameObject1);
		
		return true;
	}
	
	@Override
	public boolean mapCollisionDownEvent(GameObject gameObject1, GameObject gameObject2)
	{		
		if(gameObject2.getGameObjectType().equals(GameObjectType.CANNON))
		{
			if((GameObject)((Shot)gameObject1).getOwner() == gameObject2)
			{
				return false;
			}
		}
		
		shotsToRemove.add(gameObject1);
		
		return true;
	}
	
	@Override
	public boolean creatureCollisionEvent(GameObject creature1, GameObject creature2)
	{				
		if(creature2.getGameObjectType().equals(GameObjectType.PLAYER) ||
		   creature2.getGameObjectType().equals(GameObjectType.ENEMY))
		{
			ControllerBase bobController = creature2.getController();
			
			if(bobController != null)
			{
				if(bobController.creatureCollisionEvent(creature1, creature2))
				{
					shotsToRemove.add(creature1);
				}
			}
		}
		
		return true;
	}
}
