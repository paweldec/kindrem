package com.pde.game;

import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.pde.game.Blood.BloodType;
import com.pde.game.GameObject.GameObjectType;
import com.pde.game.GameObject.State;
import com.pde.game.Level.Layer;
import com.pde.game.Utils.Direction;

public class EnemyController extends ControllerBase
{
	private static final float MAX_VEL = 4.0f;
	private static final float GRAVITY = -20.0f;
	private static final float DAMP = 0.90f;
	private static final float ACCELERATION = 20.0f;
	
	private Enemy enemy;
	
	private boolean grounded = false;
	private boolean moveRight = true;
	
	private Random randomNumGen = new Random();
	
	public EnemyController(World world, Enemy enemy)
	{
		super(world);
		this.enemy = enemy;
		this.enemy.setController(this);
	}

	@Override
	public void update(float delta)
	{
		//System.out.println("Enemy position: " + enemy.getPosition());
		
		if(randomNumGen.nextInt(100) >= 98)
		{
			if(enemy.isFacingLeft())
			{
				world.addShot(enemy.getBounds().x, (enemy.getBounds().y + (enemy.getBounds().height / 2.0f)), Direction.LEFT, enemy);
			}
			else
			{
				world.addShot((enemy.getBounds().x + enemy.getBounds().width), (enemy.getBounds().y + (enemy.getBounds().height / 2.0f)), Direction.RIGHT, enemy);
			}
		}
		
		if(!enemy.isFacingLeft())
		{
			if(world.getLevel().get((int)(enemy.getBounds().x + enemy.getBounds().width + 0.2f), (int)(enemy.getPosition().y - 0.5f), Layer.LEVEL) == null)
			{
				moveRight = false;
			}
		}
		else
		{
			if(world.getLevel().get((int)(enemy.getBounds().x - 0.2f), (int)(enemy.getPosition().y - 0.5f), Layer.LEVEL) == null)
			{
				moveRight = true;
			}
		}
		
		if(grounded && enemy.getState().equals(State.JUMPING))
		{
			enemy.setState(State.IDLE);
		}
		
		enemy.getAcceleration().y = GRAVITY;
		enemy.getAcceleration().mul(delta);
		enemy.getVelocity().add(enemy.getAcceleration().x, enemy.getAcceleration().y);
		
		updateMapCollisionEvents(enemy, delta);
		
		if(moveRight)
		{
			enemy.getAcceleration().x = ACCELERATION;
			enemy.setFacingLeft(false);
			enemy.setState(State.WALKING);
		}
		else
		{
			enemy.setFacingLeft(true);
			enemy.getAcceleration().x = -ACCELERATION;
		}

		enemy.getVelocity().x *= DAMP;
		
		if(enemy.getVelocity().x > MAX_VEL)
		{
			enemy.getVelocity().x = MAX_VEL;
		}
		
		if(enemy.getVelocity().x < -MAX_VEL)
		{
			enemy.getVelocity().x = -MAX_VEL;
		}
		
		enemy.update(delta);
	}
	
	@Override
	public boolean mapCollisionLeftEvent(GameObject gameObject1, GameObject gameObject2)
	{
		enemy.getVelocity().x = 0;
		
		if(!moveRight)
		{
			moveRight = true;
		}
		
		return true;
	}
	
	@Override
	public boolean mapCollisionRightEvent(GameObject gameObject1, GameObject gameObject2)
	{		
		enemy.getVelocity().x = 0;
		
		if(moveRight)
		{
			moveRight = false;
		}
		
		return true;
	}
	
	@Override
	public boolean mapCollisionUpEvent(GameObject gameObject1, GameObject gameObject2)
	{
		enemy.getVelocity().y = 0;
		return true;
	}
	
	@Override
	public boolean mapCollisionDownEvent(GameObject gameObject1, GameObject gameObject2)
	{
		grounded = true;
		enemy.getVelocity().y = 0;
		
		return true;
	}
	
	@Override
	public boolean creatureCollisionEvent(GameObject gameObject1, GameObject gameObject2)
	{		
		if(gameObject1.getGameObjectType().equals(GameObjectType.SHOT))
		{
			if(((Shot)gameObject1).getOwner().getGameObjectType().equals(GameObjectType.PLAYER))
			{
				deathEvent();
				world.addBlood(gameObject1.getPosition().x, gameObject1.getPosition().y, Direction.RIGHT, BloodType.SHOT);
				return true;
			}
		}
		
		if(gameObject2.getGameObjectType().equals(GameObjectType.SHOT))
		{
			if(((Shot)gameObject2).getOwner().getGameObjectType().equals(GameObjectType.PLAYER))
			{
				deathEvent();
				world.addBlood(gameObject2.getBounds().x + (gameObject2.getBounds().width / 2.0f), 
						gameObject2.getPosition().y + (gameObject2.getBounds().height / 2.0f), 
						Direction.RIGHT, BloodType.SHOT);
				return true;
			}
		}
		
		return false;
	}
	
	public boolean deathEvent()
	{
		enemy.setPosition(enemy.getStartPosX(), enemy.getStartPosY());
		
		return true;
	}
}
