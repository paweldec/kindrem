package com.pde.game;

public class TouchPanelController extends ControllerBase
{
	private TouchPanel touchPanel;
	
	protected TouchPanelController(World world, TouchPanel touchPanel)
	{
		super(world);
		this.touchPanel = touchPanel;
	}

	@Override
	public void update(float delta)
	{
		
	}
	
	@Override
	public boolean touchDownEvent(int screenX, int screenY, int screenWidth, int screenHeight, int pointer, int button)
	{
		float x = screenX / Settings.WIDTH_UNIT_SIZE;
		float y = (screenHeight - screenY) / Settings.HEIGHT_UNIT_SIZE;
		
		System.out.println("Touch Panel Controller => screenX: " + x + " screenY: " + y);
		
		BobController bobController = (BobController)ControllerBase.getController("bobController");
		
		if(bobController == null)
		{
			return false;
		}
		
		boolean used = false;
		
		System.out.println("cnt ok");
		
		if(touchPanel.getLeftTouchArea().contains(x, y))
		{
			System.out.println("l");
			bobController.leftPressed();
			used = true;
		}
		
		if(touchPanel.getRightTouchArea().contains(x, y))
		{
			System.out.println("r");
			bobController.rightPressed();
			used = true;
		}
		
		if(touchPanel.getaTouchArea().contains(x, y))
		{
			System.out.println("a");
			bobController.jumpPressed();
			used = true;
		}
		
		if(touchPanel.getbTouchArea().contains(x, y))
		{
			System.out.println("b");
			bobController.firePressed();
			used = true;
		}
		
		return used;
	}

	@Override
	public boolean touchUpEvent(int screenX, int screenY, int screenWidth, int screenHeight, int pointer, int button)
	{
		float x = screenX / Settings.WIDTH_UNIT_SIZE;
		float y = (screenHeight - screenY) / Settings.HEIGHT_UNIT_SIZE;
		
		BobController bobController = (BobController)ControllerBase.getController("bobController");
		
		if(bobController == null)
		{
			return false;
		}
		
		boolean used = false;
		
		if(touchPanel.getLeftTouchArea().contains(x, y))
		{
			bobController.leftReleased();
			used = true;
		}
		
		if(touchPanel.getRightTouchArea().contains(x, y))
		{
			bobController.rightReleased();
			used = true;
		}
		
		if(touchPanel.getaTouchArea().contains(x, y))
		{
			bobController.jumpReleased();
			used = true;
		}
		
		if(touchPanel.getbTouchArea().contains(x, y))
		{
			bobController.fireReleased();
			used = true;
		}
		
		return used;
	}
}
