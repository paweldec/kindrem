package com.pde.game;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.pde.game.Blood.BloodType;
import com.pde.game.GameObject.GameObjectType;
import com.pde.game.GameObject.State;
import com.pde.game.Item.ItemType;
import com.pde.game.Utils.Direction;
import com.badlogic.gdx.Input.Keys;

public class BobController extends ControllerBase
{
	private static final long LONG_JUMP_PRESS = 150l;
	private static final float ACCELERATION = 20.0f;
	private static final float GRAVITY = -20.0f;
	private static final float MAX_JUMP_SPEED = 7.0f;
	private static final float DAMP = 0.90f;
	private static final float MAX_VEL = 4.0f;
	private static final double HANG_MAX_DISTANCE = 0.1;
	
	enum BobKeys
	{
		LEFT,
		RIGHT,
		UP,
		DOWN,
		JUMP,
		FIRE,
		HANG
	}
	
	private Bob bob;
	
	private long jumpPressedTime;
	private boolean jumpingPressed;
	private boolean firePressed;
	
	private boolean grounded = false;
	
	private boolean canHang = false;
	private boolean onLadder = false;
	
	static Map<BobKeys, Boolean> keys = new HashMap<BobKeys, Boolean>();
	static 
	{
		keys.put(BobKeys.LEFT, false);
		keys.put(BobKeys.RIGHT, false);
		keys.put(BobKeys.UP, false);
		keys.put(BobKeys.DOWN, false);
		keys.put(BobKeys.JUMP, false);
		keys.put(BobKeys.FIRE, false);
		keys.put(BobKeys.HANG, false);
	}
	
	public BobController(World world)
	{
		super(world);
		this.bob = world.getBob();
		this.bob.setController(this);
	}
	
	public void leftPressed()
	{
		keys.get(keys.put(BobKeys.LEFT, true));
	}
	
	public void rightPressed()
	{
		keys.get(keys.put(BobKeys.RIGHT, true));
	}
	
	private void upPressed()
	{
		keys.get(keys.put(BobKeys.UP, true));
	}
	
	private void downPressed()
	{
		keys.get(keys.put(BobKeys.DOWN, true));
	}
	
	public void jumpPressed()
	{
		keys.get(keys.put(BobKeys.JUMP, true));
	}
	
	public void firePressed()
	{
		keys.get(keys.put(BobKeys.FIRE, true));
	}
	
	public void leftReleased()
	{
		keys.get(keys.put(BobKeys.LEFT, false));
	}
	
	public void rightReleased()
	{
		keys.get(keys.put(BobKeys.RIGHT, false));
	}
	
	private void upReleased()
	{
		keys.get(keys.put(BobKeys.UP, false));
	}
	
	private void downReleased()
	{
		keys.get(keys.put(BobKeys.DOWN, false));
	}
	
	public void jumpReleased()
	{
		keys.get(keys.put(BobKeys.JUMP, false));
		jumpingPressed = false;
	}
	
	public void fireReleased()
	{
		keys.get(keys.put(BobKeys.FIRE, false));
	}
	
	private void processInput()
	{
		if(keys.get(BobKeys.FIRE) && !firePressed)
		{
			firePressed = true;
			
			if(bob.isFacingLeft())
			{
				world.addShot(bob.getBounds().x, (bob.getBounds().y + (bob.getBounds().height / 2.0f)), Direction.LEFT, bob);
			}
			else
			{
				world.addShot((bob.getBounds().x + bob.getBounds().width), (bob.getBounds().y + (bob.getBounds().height / 2.0f)), Direction.RIGHT, bob);
			}
		}
		else if(!keys.get(BobKeys.FIRE))
		{
			firePressed = false;
		}
		
		if(keys.get(BobKeys.JUMP))
		{			
			if(!bob.getState().equals(State.JUMPING))
			{							
				jumpingPressed = true;
				onLadder = false;
				jumpPressedTime = System.currentTimeMillis();
				bob.setState(State.JUMPING);
				bob.getVelocity().y = MAX_JUMP_SPEED;
				grounded = false;
			}
			else
			{
				if(jumpingPressed && ((System.currentTimeMillis() - jumpPressedTime) >= LONG_JUMP_PRESS))
				{
					jumpingPressed = false;
				}
				else
				{					
					if(jumpingPressed)
					{
						bob.getVelocity().y = MAX_JUMP_SPEED;
					}
				}
			}
		}
		
		if(keys.get(BobKeys.LEFT))
		{
			if(onLadder)
			{
				bob.getAcceleration().x = -ACCELERATION * 5.0f;
			}
			else
			{
				bob.setFacingLeft(true);
				
				if(!bob.getState().equals(State.JUMPING))
				{
					bob.setState(State.WALKING);
				}
				
				bob.getAcceleration().x = -ACCELERATION;
			}
		}
		else if(keys.get(BobKeys.RIGHT))
		{
			if(onLadder)
			{
				bob.getAcceleration().x = ACCELERATION * 5.0f;
			}
			else
			{
				bob.setFacingLeft(false);
				
				if(!bob.getState().equals(State.JUMPING))
				{
					bob.setState(State.WALKING);
				}
				
				bob.getAcceleration().x = ACCELERATION;				
			}
		}
		else if(keys.get(BobKeys.UP))
		{
			if(onLadder)
			{
				bob.getAcceleration().y = ACCELERATION * 5.0f;
			}
		}
		else if(keys.get(BobKeys.DOWN))
		{
			if(onLadder)
			{
				bob.getAcceleration().y = -ACCELERATION * 5.0f;
			}
		}
		else if((keys.get(BobKeys.LEFT) && keys.get(BobKeys.RIGHT)) || 
				(!keys.get(BobKeys.LEFT) && !keys.get(BobKeys.RIGHT) 
				&& !keys.get(BobKeys.UP) && !keys.get(BobKeys.DOWN)))
		{
			if(!bob.getState().equals(State.JUMPING))
			{
				bob.setState(State.IDLE);
			}
			
			bob.getAcceleration().x = 0;
			bob.getVelocity().x = 0;
		}
	}
	
	public void update(float delta)
	{
		//System.out.println("Bob position: " + bob.getPosition());
		
		if(grounded && bob.getState().equals(State.JUMPING))
		{
			bob.setState(State.IDLE);
		}

		if(!onLadder)
		{
			bob.getAcceleration().y = GRAVITY;
			bob.getAcceleration().mul(delta);
			bob.getVelocity().add(bob.getAcceleration().x, bob.getAcceleration().y);
		}
		else
		{
			bob.getAcceleration().mul(delta);
			bob.getVelocity().add(bob.getAcceleration().x, bob.getAcceleration().y);
			bob.getAcceleration().x = 0.0f;
			bob.getAcceleration().y = 0.0f;
		}
		
		onLadder = false;
		grounded = false;
		
		updateMapCollisionEvents(bob, delta);
		updateCreatureCollisionEvents(bob, world.getEnemies());
		updateCreatureCollisionEvents(bob, world.getCannons());
		
		processInput();

		bob.getVelocity().x *= DAMP;
		
		if(bob.getVelocity().x > MAX_VEL)
		{
			bob.getVelocity().x = MAX_VEL;
		}
		
		if(bob.getVelocity().x < -MAX_VEL)
		{
			bob.getVelocity().x = -MAX_VEL;
		}
		
		if(onLadder)
		{
			bob.getVelocity().x = 0.0f;
			bob.getVelocity().y = 0.0f;
			
			// bob.setState(State.LADDER);
		}
		
		bob.update(delta);
	}
	
	@Override
	public boolean keyDownEvent(int keycode)
	{
		if(keycode == Keys.LEFT)
		{
			leftPressed();
		}
		
		if(keycode == Keys.RIGHT)
		{
			rightPressed();
		}
		
		if(keycode == Keys.UP)
		{
			upPressed();
		}
		
		if(keycode == Keys.DOWN)
		{
			downPressed();
		}
		
		if(keycode == Keys.Z)
		{
			jumpPressed();
		}
		
		if(keycode == Keys.X)
		{
			firePressed();
		}
		
		return true;
	}
	
	@Override
	public boolean keyUpEvent(int keycode)
	{
		if(keycode == Keys.LEFT)
		{
			leftReleased();
		}
		
		if(keycode == Keys.RIGHT)
		{
			rightReleased();
		}
		
		if(keycode == Keys.UP)
		{
			upReleased();
		}
		
		if(keycode == Keys.DOWN)
		{
			downReleased();
		}
		
		if(keycode == Keys.Z)
		{
			jumpReleased();
		}
		
		if(keycode == Keys.X)
		{
			fireReleased();
		}
		
		return true;
	}
	/*
	@Override
	public boolean touchDownEvent(int screenX, int screenY, int screenWidth, int screenHeight, int pointer, int button)
	{		
		if(screenX < screenWidth / 2 && screenY > screenHeight / 2)
		{
			leftPressed();
		}
		
		if(screenX > screenWidth / 2 && screenY > screenHeight / 2)
		{
			rightPressed();
		}

		return true;
	}
	
	@Override
	public boolean touchUpEvent(int screenX, int screenY, int screenWidth, int screenHeight, int pointer, int button)
	{		
		if(screenX < screenWidth / 2 && screenY > screenHeight / 2)
		{
			leftReleased();
		}
		
		if(screenX > screenWidth / 2 && screenY > screenHeight / 2)
		{
			rightReleased();
		}

		return true;
	}
	*/
	@Override
	public boolean mapCollisionLeftEvent(GameObject gameObject1, GameObject gameObject2)
	{
		//if(!bob.getState().equals(State.JUMPING))
		{
			if(gameObject2.getGameObjectType().equals(GameObjectType.BLOCK))
			{
				if(((Block)gameObject2).isLadder())
				{				
					onLadder = true;
					
					return true;
				}
			}
		}
		
		bob.getVelocity().x = 0;		
		return true;
	}
	
	@Override
	public boolean mapCollisionRightEvent(GameObject gameObject1, GameObject gameObject2)
	{
		//if(!bob.getState().equals(State.JUMPING))
		{
			if(gameObject2.getGameObjectType().equals(GameObjectType.BLOCK))
			{
				if(((Block)gameObject2).isLadder())
				{				
					onLadder = true;
					
					return true;
				}
			} 
		}
		
		bob.getVelocity().x = 0;
		return true;
	}
	
	@Override
	public boolean mapCollisionUpEvent(GameObject gameObject1, GameObject gameObject2)
	{
		//if(!bob.getState().equals(State.JUMPING))
		{
			if(gameObject2.getGameObjectType().equals(GameObjectType.BLOCK))
			{
				if(((Block)gameObject2).isLadder())
				{				
					onLadder = true;
					
					return true;
				}
			}
		}
		
		bob.getVelocity().y = 0;
		return true;
	}
	
	@Override
	public boolean mapCollisionDownEvent(GameObject gameObject1, GameObject gameObject2)
	{
		//if(!bob.getState().equals(State.JUMPING))
		{
			if(gameObject2.getGameObjectType().equals(GameObjectType.BLOCK))
			{
				if(((Block)gameObject2).isLadder())
				{								
					onLadder = true;
		
					return true;
				}
			}
		}
		
		grounded = true;
		bob.getVelocity().y = 0;
		return true;
	}
	
	public boolean deathEvent()
	{		
		int life = bob.getLife();
		
		if(life > 1)
		{
			life--;
			bob.setLife(life);
		}
		else
		{
			bob.setLife(bob.getMaxLife());
			bob.setPosition(bob.getStartPosX(), bob.getStartPosY());
		}
		
		return true;
	}
	
	@Override
	public boolean creatureCollisionEvent(GameObject gameObject1, GameObject gameObject2)
	{
		if(gameObject1.getGameObjectType().equals(GameObjectType.SHOT))
		{
			if(((Shot)gameObject1).getOwner().getGameObjectType().equals(GameObjectType.PLAYER))
			{
				return false;
			}
			
			world.addBlood(gameObject2.getPosition().x, gameObject2.getPosition().y, Direction.RIGHT, BloodType.SHOT);
		}
		
		if(gameObject2.getGameObjectType().equals(GameObjectType.SHOT))
		{
			if(((Shot)gameObject2).getOwner().getGameObjectType().equals(GameObjectType.PLAYER))
			{
				return false;
			}
			
			world.addBlood(gameObject2.getPosition().x, gameObject2.getPosition().y, Direction.RIGHT, BloodType.SHOT);
		}
		else if(gameObject2.getGameObjectType().equals(GameObjectType.ITEM))
		{
			Item item = (Item)gameObject2;
			
			if(item.getItemType().equals(ItemType.ITEM_DOOR_GREEN) ||
			   item.getItemType().equals(ItemType.ITEM_DOOR_BROWN) ||
			   item.getItemType().equals(ItemType.ITEM_DOOR_SILVER) ||
			   item.getItemType().equals(ItemType.ITEM_DOOR_BROWN))
			{
				if(!item.isDoorOpen())
				{
					if(bob.tryOpenDoor(item))
					{
						item.setDoorOpened(true);
					}
				}
				
				if(!item.isDoorOpen())
				{
					bob.getVelocity().x = -(bob.getVelocity().x * 2.0f);
				}
				
				return true;
			}
		}
		
		
		
		/*
		if(gameObject2.getGameObjectType().equals(GameObjectType.CANNON))
		{			
			Vector2 bobCentralPoint = Utils.getCentralPointInRect(bob.getBounds());
			Vector2 cannonCentralPoint = Utils.getCentralPointInRect(gameObject2.getBounds());
			
			boolean left = ((bobCentralPoint.x - cannonCentralPoint.x) < 0);
			boolean up = ((bobCentralPoint.y - cannonCentralPoint.y) < 0);
			
			if(left)
			{
				bob.getVelocity().x = 0;
			}
			
			if(up)
			{
				bob.getVelocity().y = 0;
			}
			
		}
		*/
		
		deathEvent();
		
		return true;
	}
}
