package com.pde.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class Kindrem extends Game
{	
	@Override
	public void create()
	{		
		SpriteManager.load(Gdx.files.internal("sprites/config.xml"));
		setScreen(new GameScreen());
	}
}
