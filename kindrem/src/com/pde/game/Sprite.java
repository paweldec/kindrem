package com.pde.game;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Sprite
{	
	private String currentAnimation = "null";
	private String name = "null";
	private Map<String, Animation> data = new HashMap<String, Animation>();
	
	private long _lastTimeStamp;
	
	public Sprite()
	{
		_lastTimeStamp = System.currentTimeMillis();
	}
	
	public Sprite(Sprite sprite)
	{
		_lastTimeStamp = System.currentTimeMillis();
		
		data = new HashMap<String, Animation>();
		
		for(Entry<String, Animation> entry : sprite.data.entrySet())
		{
			data.put(new String(entry.getKey()), new Animation(entry.getValue()));
		}
		
		currentAnimation = new String(sprite.currentAnimation);
		name = new String(sprite.name);
	}
	
	public void update(float delta)
	{		
		Animation animation = data.get(currentAnimation);
		
		if((_lastTimeStamp + animation.getDelay()) < System.currentTimeMillis())
		{			
			_lastTimeStamp = System.currentTimeMillis();
			int currentFrame = animation.getCurrentFrame(); 
			
			if(animation.getFrames().size() - 1 > currentFrame)
			{				
				currentFrame++;
				animation.setCurrentFrame(currentFrame);
			}
			else
			{
				if(animation.getLoop())
				{
					animation.setCurrentFrame(0);
				}
			}
		}
	}
	
	public void setAnimation(String name)
	{		
		if(currentAnimation.equals(name)) return;
		
		if(data.containsKey(name))
		{			
			currentAnimation = name;
			data.get(currentAnimation).setCurrentFrame(0);
		}
		else
		{
			System.out.println("animation not found: " + name);
			System.out.println("available animations:");
			
			for(Entry<String, Animation> entry : data.entrySet())
			{
				System.out.println(entry.getKey());
			}
		}
	}
	
	public void addAnimation(String name, Animation animation)
	{		
		if(animation == null)
		{
			System.out.println("Animation is nulled: " + name);
		}
		
		data.put(name, animation);
		setAnimation(name);
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public TextureRegion getCurrentFrame()
	{
		Animation animation = data.get(currentAnimation);
		TextureRegion frame = animation.getFrames().get(animation.getCurrentFrame());
		
		if(frame == null)
		{
			System.out.println("Sprite frame == null. Sprite name: " + name + " animation:" + currentAnimation + " frame:" + animation.getCurrentFrame() + 1);
		}
		
		return frame;
	}
}
