package com.pde.game;

public class Settings
{
	public static boolean DEBUG_MODE = false;
	public static boolean EDITOR_MODE = false;
	
	public static float WIDTH_UNIT_SIZE = 48.0f;
	public static float HEIGHT_UNIT_SIZE = 45.7f;
	
	//public static float EDITOR_CLEAR_OBJECT_MAX_DISTANCE = 1.0f;
	public static boolean TOUCH_CONTROL_PANEL = true;
}
