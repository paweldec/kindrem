package com.pde.game;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Utils
{
	public enum Direction
	{
		UP,
		DOWN,
		LEFT,
		RIGHT
	}
	
	/**
	 * Zaokrągla float do 2 miejsc po przecinku
	 */
	public static float roundFloat(float num)
	{
		num = num * 100;
	    int A = (int)num;
	    num = A;
	    num = num / 100;
	    return num;
	}
	
	/**
	 * Zwraca odleglosc pomiedzy dwoma kwadratami
	 */
	public static double calcDistanceBetweenRects(Rectangle rect1, Rectangle rect2)
	{
		Vector2 p1 = new Vector2();
		Vector2 p2 = new Vector2();
		
		p1.x = (rect1.x + (rect1.width / 2.0f));
		p1.y = (rect1.y + (rect1.height / 2.0f));
		p2.x = (rect2.x + (rect2.width / 2.0f));
		p2.y = (rect2.y + (rect2.height / 2.0f));
		
		return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
	}
	
	/**
	 * Zwraca odleglosc pomiedzy dwoma punktami
	 */
	public static double calcDistanceBetweenPoints(float x1, float y1, float x2, float y2)
	{
		return Math.sqrt((x1 - x2) *  (x1 - x2) + (y1 - y2) *  (y1 - y2));
	}
	
	/**
	 * Konwertuje string na boolean
	 */
	public static boolean stringToBool(String str)
	{
		str = str.toLowerCase();
		
		if(str.equals("true"))
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Konwertuje String na Direction
	 */
	public static Direction stringToDirection(String str)
	{
		str = str.toLowerCase();
		Direction direction = null;
		
		if(str.equals("left"))
		{
			direction = Direction.LEFT;
		}
		else if(str.equals("right"))
		{
			direction = Direction.RIGHT;
		}
		else if(str.equals("up"))
		{
			direction = Direction.UP;
		}
		else if(str.equals("down"))
		{
			direction = Direction.DOWN;
		}
		
		return direction;
	}
	
	/**
	 * Pobiera centralny punkt prostokąta
	 */
	public static Vector2 getCentralPointInRect(Rectangle rect)
	{
		Vector2 point = new Vector2();
		
		point.x = rect.x + (rect.width / 2.0f);
		point.y = rect.y + (rect.height / 2.0f);
		
		return point;
	}
}
