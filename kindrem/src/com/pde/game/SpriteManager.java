package com.pde.game;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.XmlReader;

public class SpriteManager
{	
	private static SpriteLoader spriteLoader = new SpriteLoader();
	private static Map<String, Sprite> spritesData = new HashMap<String, Sprite>();
	private static Map<String, Vector<Sprite>> spritesInUse = new HashMap<String, Vector<Sprite>>();
	
	public static void load(FileHandle fileHandle)
	{
		XmlReader xmlReader = new XmlReader();
		XmlReader.Element xmlConfigElement = null;
		
		try 
		{
			xmlConfigElement = xmlReader.parse(fileHandle);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		for(int i = 0; i < xmlConfigElement.getChildCount(); i++)
		{
			XmlReader.Element xmlSpriteElement = xmlConfigElement.getChild(i);
			String spriteFilePath = xmlSpriteElement.getAttribute("path");
			
			Sprite sprite = spriteLoader.loadSprite(Gdx.files.internal(spriteFilePath));
			
			if(sprite != null)
			{
				spritesData.put(sprite.getName(), sprite);
			}
		}
	}
	
	public static Sprite getSprite(String name)
	{
		if(spritesData.containsKey(name))
		{
			Sprite sprite = new Sprite(spritesData.get(name));
			
			if(!spritesInUse.containsKey(name))
			{
				spritesInUse.put(name, new Vector<Sprite>());
			}
			
			Vector<Sprite> list = spritesInUse.get(name);
			list.add(sprite);
			
			return sprite;
		}
		
		return null;
	}
	
	public static void update(float delta)
	{
		for(Map.Entry<String, Vector<Sprite>> entry : spritesInUse.entrySet()) 
		{
			for(Sprite sprite : entry.getValue())
			{
				sprite.update(delta);
			}
		}
	}
}
