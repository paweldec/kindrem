package com.pde.game;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;


public class GameScreen implements Screen, InputProcessor
{
	private World world;
	private WorldRenderer renderer;
	private EditorGUI editorGUI;
	
	private int width, height;
	
	private long _lastTime = 0L;
	private long _currentTime = 0L;
	
	@Override
	public void render(float delta)
	{
		_currentTime = System.currentTimeMillis();
		
		if(_lastTime + 25L < _currentTime)
		{			
			delta = (_currentTime - _lastTime) / 1000.0f;
			
			if(delta > 0.04)
			{
				delta = 0.04f;
			}
			
			for (Map.Entry<String, ControllerBase> entry : ControllerBase.getAllControllers().entrySet()) 
			{
				entry.getValue().update(delta);
			}
			
			SpriteManager.update(delta);
			
			_lastTime = System.currentTimeMillis();
			
			System.out.println("Current time " + System.currentTimeMillis());
			System.out.println("Delta " + delta);
			System.out.println("FPS " + (delta * 1000));
		}
		
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		renderer.render();	
	
		if(Settings.EDITOR_MODE)
		{
			editorGUI.render();
		}
	}

	@Override
	public void resize(int width, int height)
	{
		renderer.setSize(width, height);
		this.width = width;
		this.height = height;
	}

	@Override
	public void show()
	{
		world = new World();
		renderer = new WorldRenderer(world);
		
		if(Settings.EDITOR_MODE)
		{
			editorGUI = new EditorGUI(world);
		}
		
		ControllerBase.addController("bobController", new BobController(world));
		
		
		int enemyCounter = 0;
		
		for(Enemy enemy : world.getEnemies())
		{
			ControllerBase.addController("enemyController" + enemyCounter, new EnemyController(world, enemy));
			enemyCounter++;
		}
		
		
		ControllerBase.addController("cameraController", new CameraController(world, renderer.getCamera()));
		ControllerBase.addController("shotController", new ShotController(world));
		
		int cannonCounter = 0;
		
		for(Cannon cannon : world.getCannons())
		{
			ControllerBase.addController("cannonController" + cannonCounter, new CannonController(world, cannon));
			cannonCounter++;
		}
		
		int platformCounter = 0;
		
		for(Platform platform : world.getPlatforms())
		{
			ControllerBase.addController("platformController" + platformCounter, new PlatformController(world, platform));
			platformCounter++;
		}
		
		ControllerBase.addController("itemController", new ItemController(world));
		ControllerBase.addController("bloodController", new BloodController(world));
		
		if(Settings.TOUCH_CONTROL_PANEL)
		{
			ControllerBase.addController("touchPanelController", new TouchPanelController(world, world.getTouchPanel()));		
		}
		
		// edytor musi byc tworzony na koncu
		ControllerBase.addController("editorController", new EditorController(world, world.getEditor()));
		
		Gdx.input.setInputProcessor(this);
		
		
		// test
		//System.out.println("Map loading...");
		//MapLoader mapLoader = new MapLoader(world);
		//mapLoader.loadMap(Gdx.files.external("Kindrem/maps/dev_map01.xml"));
		//
	}

	@Override
	public void hide()
	{
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause()
	{
		
	}

	@Override
	public void resume()
	{
		
	}

	@Override
	public void dispose()
	{
		
	}

	@Override
	public boolean keyDown(int keycode)
	{
		if(Settings.EDITOR_MODE)
		{
			if(editorGUI.keyDown(keycode))
			{
				return true;
			}
		}
		
		boolean used = false;
		
		Iterator<String> it = ControllerBase.getAllControllers().keySet().iterator();

		while(it.hasNext())
		{
			String key = it.next();
			
			if(ControllerBase.getAllControllers().get(key).keyDownEvent(keycode) && !used)
			{
				used = true;
			}
		}
		
		return used;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		if(Settings.EDITOR_MODE)
		{
			if(editorGUI.keyUp(keycode))
			{
				return true;
			}
		}
		
		boolean used = false;		
		
		Iterator<String> it = ControllerBase.getAllControllers().keySet().iterator();

		while(it.hasNext())
		{
			String key = it.next();
			
			if(ControllerBase.getAllControllers().get(key).keyUpEvent(keycode) && !used)
			{
				used = true;
			}
		}
		
		return used;
	}

	@Override
	public boolean keyTyped(char character)
	{
		if(Settings.EDITOR_MODE)
		{
			if(editorGUI.keyTyped(character))
			{
				return true;
			}
		}
		
		boolean used = false;
		
		Iterator<String> it = ControllerBase.getAllControllers().keySet().iterator();

		while(it.hasNext())
		{
			String key = it.next();
			
			if(ControllerBase.getAllControllers().get(key).keyTypedEvent(character) && !used)
			{
				used = true;
			}
		}
		
		return used;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		if(Settings.EDITOR_MODE)
		{
			if(editorGUI.touchDown(screenX, screenY, pointer, button))
			{
				return true;
			}
		}
		
		boolean used = false;

		Iterator<String> it = ControllerBase.getAllControllers().keySet().iterator();

		while(it.hasNext())
		{
			String key = it.next();
			
			if(ControllerBase.getAllControllers().get(key).touchDownEvent(screenX, screenY, width, height, pointer, button) && !used)
			{
				used = true;
			}
		}
		
		return used;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		if(Settings.EDITOR_MODE)
		{
			if(editorGUI.touchUp(screenX, screenY, pointer, button))
			{
				return true;
			}
		}
		
		boolean used = false;
		
		Iterator<String> it = ControllerBase.getAllControllers().keySet().iterator();

		while(it.hasNext())
		{
			String key = it.next();
			
			if(ControllerBase.getAllControllers().get(key).touchUpEvent(screenX, screenY, width, height, pointer, button) && !used)
			{
				used = true;
			}
		}
		
		return used;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		if(Settings.EDITOR_MODE)
		{
			if(editorGUI.touchDragged(screenX, screenY, pointer))
			{
				return true;
			}
		}
		
		boolean used = false;
		
		Iterator<String> it = ControllerBase.getAllControllers().keySet().iterator();

		while(it.hasNext())
		{
			String key = it.next();
			
			if(ControllerBase.getAllControllers().get(key).touchDraggedEvent(screenX, screenY, pointer) && !used)
			{
				used = true;
			}
		}
		
		return used;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		if(Settings.EDITOR_MODE)
		{
			if(editorGUI.mouseMoved(screenX, screenY))
			{
				return true;
			}
		}
		
		boolean used = false;
				
		Iterator<String> it = ControllerBase.getAllControllers().keySet().iterator();

		while(it.hasNext())
		{
			String key = it.next();
			
			if(ControllerBase.getAllControllers().get(key).mouseMovedEvent(screenX, screenY) && !used)
			{
				used = true;
			}
		}
		
		return used;
	}

	@Override
	public boolean scrolled(int amount)
	{
		if(Settings.EDITOR_MODE)
		{
			if(editorGUI.scrolled(amount))
			{
				return true;
			}
		}
		
		boolean used = false;
		
		Iterator<String> it = ControllerBase.getAllControllers().keySet().iterator();

		while(it.hasNext())
		{
			String key = it.next();
			
			if(ControllerBase.getAllControllers().get(key).scrolledEvent(amount) && !used)
			{
				used = true;
			}
		}
		
		return used;
	}
	
}
