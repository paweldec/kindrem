package com.pde.game;

import java.util.Vector;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Animation
{		
	private boolean loop;
	private Vector<TextureRegion> frames;
	private long delay;
	private int currentFrame;
	
	public Animation(boolean loop, long delay, Vector<TextureRegion> frames)
	{
		this.loop = loop;
		this.delay = delay;
		this.currentFrame = 0;
		this.frames = frames;
	}
	
	public Animation(Animation animation)
	{
		loop = animation.loop;
		delay = animation.delay;
		currentFrame = animation.currentFrame;
		
		frames = new Vector<TextureRegion>();
		
		for(TextureRegion textureRegion: animation.frames)
		{
			frames.add(new TextureRegion(textureRegion));
		}
	}

	public Vector<TextureRegion> getFrames()
	{
		return frames;
	}
	
	public void setCurrentFrame(int frame)
	{
		this.currentFrame = frame;
	}
	
	public int getCurrentFrame()
	{
		return currentFrame;
	}
	
	public long getDelay()
	{
		return delay;
	}
	
	public boolean getLoop()
	{
		return loop;
	}
}