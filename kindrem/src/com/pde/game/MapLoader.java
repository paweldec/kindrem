package com.pde.game;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlWriter;
import com.pde.game.Level.Layer;
import com.pde.game.Utils.Direction;
import com.sun.org.apache.bcel.internal.classfile.Attribute;

public class MapLoader
{
	private World world;
	private XmlReader xmlReader;
	private XmlWriter xmlWriter; 
	private StringWriter writer;

	private static final int MAP_FILE_SECTION_PLAYERS = 0;
	private static final int MAP_FILE_SECTION_LEVEL = 1;
	private static final int MAP_FILE_SECTION_BACKGROUND = 2;
	private static final int MAP_FILE_SECTION_FOREGROUND = 3;
	private static final int MAP_FILE_SECTION_ENEMIES = 4;
	private static final int MAP_FILE_SECTION_CANNONS = 5;
	
	public MapLoader(World world)
	{
		this.world = world;
		xmlReader = new XmlReader();
		writer = new StringWriter();
		xmlWriter = new XmlWriter(writer);
	}
	
	public void saveMap(FileHandle fileHandle)
	{		
		try 
		{
			xmlWriter.element("map")
			.attribute("name", "dev_map01")
			.attribute("width", world.getLevel().getWidth())
			.attribute("height", world.getLevel().getHeight());
			
			xmlWriter.element("players");
			xmlWriter.element("player")
			.attribute("x", world.getBob().getStartPosX())
			.attribute("y", world.getBob().getStartPosY())
			.attribute("facing-left", world.getBob().isStartFacingLeft())
			.pop()
			.pop();
			
			Block[][] blocks = world.getLevel().getBlocks();
			
			xmlWriter.element("blocks");
			
			for(int i = 0; i < blocks.length; i++)
			{
				for(int j = 0; j < blocks[i].length; j++)
				{
					if(blocks[i][j] == null) continue;
					
					float x, y;
					x = (float)i;
					y = (float)j;
					
					xmlWriter.element("block")
					.attribute("x", x)
					.attribute("y", y)
					.attribute("type", blocks[i][j].getType())
					.attribute("collidable", blocks[i][j].isCollidable())
					.attribute("trap", blocks[i][j].isTrap())
					.attribute("ladder", blocks[i][j].isLadder())
					.pop();
				}
			}
			
			xmlWriter.pop();
			
			Block[][] background = world.getLevel().getBackground();
			
			xmlWriter.element("background");
			
			for(int i = 0; i < background.length; i++)
			{
				for(int j = 0; j < background[i].length; j++)
				{
					if(background[i][j] == null) continue;
					
					float x, y;
					x = (float)i;
					y = (float)j;
					
					xmlWriter.element("block")
					.attribute("x", x)
					.attribute("y", y)
					.attribute("type", background[i][j].getType())
					//.attribute("collidable", blocks[i][j].isCollidable())
					//.attribute("trap", blocks[i][j].isTrap())
					.pop();
				}
			}
			
			xmlWriter.pop();
			
			Block[][] foreground = world.getLevel().getForeground();
			
			xmlWriter.element("foreground");
			
			for(int i = 0; i < foreground.length; i++)
			{
				for(int j = 0; j < foreground[i].length; j++)
				{
					if(foreground[i][j] == null) continue;
					
					float x, y;
					x = (float)i;
					y = (float)j;
					
					xmlWriter.element("block")
					.attribute("x", x)
					.attribute("y", y)
					.attribute("type", foreground[i][j].getType())
					//.attribute("collidable", blocks[i][j].isCollidable())
					//.attribute("ladder", blocks[i][j].isLadder())
					.pop();
				}
			}
			
			xmlWriter.pop();
			
			xmlWriter.element("enemies");
			
			for(Enemy enemy : world.getEnemies())
			{
				xmlWriter.element("enemy")
				.attribute("x", enemy.getStartPosX())
				.attribute("y", enemy.getStartPosY())
				.attribute("facing-left", enemy.isStartFacingLeft())
				.pop();
			}
			
			xmlWriter.pop();
			
			xmlWriter.element("cannons");
			
			for(Cannon cannon : world.getCannons())
			{
				xmlWriter.element("cannon")
				.attribute("x", cannon.getStartPosX())
				.attribute("y", cannon.getStartPosY())
				.attribute("direction", cannon.getDirection())
				.attribute("shot-delay", cannon.getShotDelay())
				.pop();
			}
			
			xmlWriter.pop();
			
			
			xmlWriter.pop();
			
			System.out.println(writer.toString());
			
			fileHandle.delete();
			fileHandle.writeString(writer.toString(), false);
			
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void loadMap(FileHandle fileHandle)
	{
		XmlReader.Element xmlMapElement = null;
		
		try 
		{
			xmlMapElement = xmlReader.parse(fileHandle);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		String mapName = xmlMapElement.getAttribute("name");
		String _mapWidth = xmlMapElement.getAttribute("width");
		String _mapHeight = xmlMapElement.getAttribute("height");
		
		int mapWidth = Integer.parseInt(_mapWidth);
		int mapHeight = Integer.parseInt(_mapHeight);
		
		world.getLevel().changeSize(mapWidth, mapHeight);
		world.getLevel().setName(mapName);
		world.clear();
		
		for(int i = 0; i < xmlMapElement.getChildCount(); i++)
		{
			if(i == MAP_FILE_SECTION_PLAYERS)
			{
				XmlReader.Element xmlPlayersElement = xmlMapElement.getChild(i);
				
				XmlReader.Element xmlPlayerElement = xmlPlayersElement.getChild(0);
				String _playerX = xmlPlayerElement.getAttribute("x");
				String _playerY = xmlPlayerElement.getAttribute("y");
				String _facingLeft = xmlPlayerElement.getAttribute("facing-left");
				
				try
				{
					float playerX = Float.parseFloat(_playerX);
					float playerY = Float.parseFloat(_playerY);
					boolean playerFacingLeft = Utils.stringToBool(_facingLeft);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}				
			}
			else if(i == MAP_FILE_SECTION_LEVEL)
			{
				XmlReader.Element xmlBlocksElement = xmlMapElement.getChild(i);
				
				for(int j = 0; j < xmlBlocksElement.getChildCount(); j++)
				{
					String _blockX = xmlBlocksElement.getChild(j).getAttribute("x");
					String _blockY = xmlBlocksElement.getChild(j).getAttribute("y");
					String _type = xmlBlocksElement.getChild(j).getAttribute("type");
					String _collidable = xmlBlocksElement.getChild(j).getAttribute("collidable");
					String _trap = xmlBlocksElement.getChild(j).getAttribute("trap");
					String _ladder = xmlBlocksElement.getChild(j).getAttribute("ladder");
					
					try
					{
						float blockX = Float.parseFloat(_blockX);
						float blockY = Float.parseFloat(_blockY);
						int type = Integer.parseInt(_type);
						boolean collidable = Utils.stringToBool(_collidable);
						boolean trap = Utils.stringToBool(_trap);
						boolean ladder = Utils.stringToBool(_ladder);
						
						Block newBlock = new Block(new Vector2(blockX, blockY), type);
						newBlock.setCollidable(collidable);
						newBlock.setTrap(trap);
						newBlock.setLadder(ladder);
						
						world.addBlock((int)blockX, (int)blockY, newBlock, Layer.LEVEL);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			else if(i == MAP_FILE_SECTION_BACKGROUND)
			{
				XmlReader.Element xmlBackgroundElement = xmlMapElement.getChild(i);
				
				for(int j = 0; j < xmlBackgroundElement.getChildCount(); j++)
				{
					String _blockX = xmlBackgroundElement.getChild(j).getAttribute("x");
					String _blockY = xmlBackgroundElement.getChild(j).getAttribute("y");
					String _type = xmlBackgroundElement.getChild(j).getAttribute("type");
					
					try
					{
						float blockX = Float.parseFloat(_blockX);
						float blockY = Float.parseFloat(_blockY);
						int type = Integer.parseInt(_type);
						
						Block newBlock = new Block(new Vector2(blockX, blockY), type);
						
						world.addBlock((int)blockX, (int)blockY, newBlock, Layer.BACKGROUND);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			else if(i == MAP_FILE_SECTION_FOREGROUND)
			{
				XmlReader.Element xmlBackgroundElement = xmlMapElement.getChild(i);
				
				for(int j = 0; j < xmlBackgroundElement.getChildCount(); j++)
				{
					String _blockX = xmlBackgroundElement.getChild(j).getAttribute("x");
					String _blockY = xmlBackgroundElement.getChild(j).getAttribute("y");
					String _type = xmlBackgroundElement.getChild(j).getAttribute("type");
					
					try
					{
						float blockX = Float.parseFloat(_blockX);
						float blockY = Float.parseFloat(_blockY);
						int type = Integer.parseInt(_type);
						
						Block newBlock = new Block(new Vector2(blockX, blockY), type);
						
						world.addBlock((int)blockX, (int)blockY, newBlock, Layer.FOREGROUND);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			else if(i == MAP_FILE_SECTION_ENEMIES)
			{
				XmlReader.Element xmlEnemiesElement = xmlMapElement.getChild(i);
				
				for(int j = 0; j < xmlEnemiesElement.getChildCount(); j++)
				{
					String _enemyX = xmlEnemiesElement.getChild(j).getAttribute("x");
					String _enemyY = xmlEnemiesElement.getChild(j).getAttribute("y");
					String _facingLeft = xmlEnemiesElement.getChild(j).getAttribute("facing-left");
					
					try
					{
						float enemyX = Float.parseFloat(_enemyX);
						float enemyY = Float.parseFloat(_enemyY);
						boolean facingLeft = Utils.stringToBool(_facingLeft);
						
						Enemy newEnemey = new Enemy(new Vector2(enemyX, enemyY), facingLeft);
						world.addEnemy(newEnemey);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			else if(i == MAP_FILE_SECTION_CANNONS)
			{	
				XmlReader.Element xmlCannonsElement = xmlMapElement.getChild(i);
				
				for(int j = 0; j < xmlCannonsElement.getChildCount(); j++)
				{
					String _cannonX = xmlCannonsElement.getChild(j).getAttribute("x");
					String _cannonY = xmlCannonsElement.getChild(j).getAttribute("y");
					String _direction = xmlCannonsElement.getChild(j).getAttribute("direction");
					String _shotDelay = xmlCannonsElement.getChild(j).getAttribute("shot-delay");
					
					try
					{
						float cannonX = Float.parseFloat(_cannonX);
						float cannonY = Float.parseFloat(_cannonY);
						Direction direction = Utils.stringToDirection(_direction);
						long shotDelay = Long.parseLong(_shotDelay);
						
						Cannon newCannon = new Cannon(new Vector2(cannonX, cannonY), direction);
						newCannon.setShotDelay(shotDelay);
						
						world.addCannon(newCannon);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		}
	}
}
