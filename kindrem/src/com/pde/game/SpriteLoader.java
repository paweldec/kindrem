package com.pde.game;

import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.XmlReader;

public class SpriteLoader
{
	private XmlReader xmlReader;
	
	public SpriteLoader()
	{
		xmlReader = new XmlReader();
	}
	
	public Sprite loadSprite(FileHandle fileHandle)
	{		
		XmlReader.Element xmlSpriteElement = null;
		
		try 
		{
			xmlSpriteElement = xmlReader.parse(fileHandle);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		String spriteName = xmlSpriteElement.getAttribute("name");
		String atlasPath = xmlSpriteElement.getAttribute("atlas-path");
		
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal(atlasPath));
		
		Sprite sprite = new Sprite();
		sprite.setName(spriteName);
		
		for(int i = 0; i < xmlSpriteElement.getChildCount(); i++)
		{
			XmlReader.Element xmlAnimationElement = xmlSpriteElement.getChild(i);
			String animationName = xmlAnimationElement.getAttribute("name");
			String _loop = xmlAnimationElement.getAttribute("loop");
			String _delay = xmlAnimationElement.getAttribute("delay");
			
			String textureName = "null";
			
			Vector<TextureRegion> frames = new Vector<TextureRegion>();
			
			for(int j = 0; j < xmlAnimationElement.getChildCount(); j++)
			{
				XmlReader.Element xmlFrameElement = xmlAnimationElement.getChild(j);
				textureName = xmlFrameElement.getAttribute("name");	
				
				TextureRegion frame = atlas.findRegion(textureName);
				
				frames.add(frame);
			}
			
			boolean loop = Utils.stringToBool(_loop);
			long delay = Long.parseLong(_delay);			

			Animation animation = new Animation(loop, delay, frames);
			sprite.addAnimation(animationName, animation);
		}
		
		return sprite;
	}
}
